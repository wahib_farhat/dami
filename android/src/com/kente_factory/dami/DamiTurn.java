package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/**
 * Board data.
 * Three main points of data:
 * 1. json containing positions of all board elements
 * 2. what player the player currently owns
 * 3. the current player's turn it is
 *
 * @author Wahib
 */
public class DamiTurn {

    public static final String TAG = "EBTurn";

    public String board_data = "";
//    the one who started the match
    public String white_owner_id = "";
    public String black_owner_id = "";
//    the current turn string
    public String current_turn = "";
    public String current_moves = "";

    public String white_shape = "circle";
    public String black_shape = "circle";
    public String white_crown = "normal";
    public String black_crown = "normal";

    public DamiTurn() {
    }

    // This is the byte array we will write out to the TBMP API.
    public byte[] persist() {
        JSONObject retVal = new JSONObject();

        try {
            retVal.put("board_data", board_data);
            retVal.put("white_owner_id", white_owner_id);
            retVal.put("black_owner_id", black_owner_id);
            retVal.put("current_turn", current_turn);
            retVal.put("current_moves", current_moves);

            retVal.put("white_shape", white_shape);
            retVal.put("black_shape", black_shape);
            retVal.put("white_crown", white_crown);
            retVal.put("black_crown", black_crown);

        } catch (JSONException e) {
            Gdx.app.log("DamiTurn", "There was an issue writing JSON!", e);
        }

        String st = retVal.toString();

        Gdx.app.log(TAG, "==== PERSISTING\n" + st);

        return st.getBytes(Charset.forName("UTF-8"));
    }

    // Creates a new instance of SkeletonTurn.
    static public DamiTurn unpersist(byte[] byteArray) {

        if (byteArray == null) {
            Gdx.app.log(TAG, "Empty array---possible bug.");
            return new DamiTurn();
        }

        String st = null;
        try {
            st = new String(byteArray, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
            return null;
        }

        Gdx.app.log(TAG, "====UNPERSIST \n" + st);

        DamiTurn retVal = new DamiTurn();

        try {
            JSONObject obj = new JSONObject(st);

            if (obj.has("board_data")) {
                retVal.board_data = obj.getString("board_data");
            }
            if (obj.has("white_owner_id")) {
                retVal.white_owner_id = obj.getString("white_owner_id");
            }
            if (obj.has("black_owner_id")) {
                retVal.black_owner_id = obj.getString("black_owner_id");
            }
            if (obj.has("current_turn")) {
                retVal.current_turn = obj.getString("current_turn");
            }
            if (obj.has("current_moves")) {
                retVal.current_moves = obj.getString("current_moves");
            }

            if (obj.has("white_shape")) {
                retVal.white_shape = obj.getString("white_shape");
            }
            if (obj.has("black_shape")) {
                retVal.black_shape = obj.getString("black_shape");
            }
            if (obj.has("white_crown")) {
                retVal.white_crown = obj.getString("white_crown");
            }
            if (obj.has("black_crown")) {
                retVal.black_crown = obj.getString("black_crown");
            }

        } catch (JSONException e) {
            Gdx.app.log("DamiTurn", "There was an issue parsing JSON! " + e);
        }

        return retVal;
    }
}
