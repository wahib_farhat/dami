package com.kente_factory.dami;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesCallbackStatusCodes;
import com.google.android.gms.games.GamesClient;
import com.google.android.gms.games.GamesClientStatusCodes;
import com.google.android.gms.games.InvitationsClient;
import com.google.android.gms.games.Player;
import com.google.android.gms.games.TurnBasedMultiplayerClient;
import com.google.android.gms.games.multiplayer.Invitation;
import com.google.android.gms.games.multiplayer.InvitationCallback;
import com.google.android.gms.games.multiplayer.Multiplayer;
import com.google.android.gms.games.multiplayer.realtime.RoomConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatch;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchConfig;
import com.google.android.gms.games.multiplayer.turnbased.TurnBasedMatchUpdateCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.logic.BoardItems;
import com.kente_factory.dami.logic.Move;

import java.util.ArrayList;
import java.util.Arrays;

public class AndroidLauncher extends AndroidApplication implements PlatformInterface {

    public static final String TAG = "DamiAndroid";

    Dami game;

    // For our intents
    private static final int RC_SIGN_IN = 9001;
    final static int RC_SELECT_PLAYERS = 10000;
    final static int RC_LOOK_AT_MATCHES = 10001;

    private Intent new_match_intent;

    private String mDisplayName;
    private String mPlayerId;

    // Client used to sign in with Google APIs
    private GoogleSignInClient mGoogleSignInClient = null;

    // Client used to interact with the TurnBasedMultiplayer system.
    private TurnBasedMultiplayerClient mTurnBasedMultiplayerClient = null;

    // Client used to interact with the Invitation system.
    private InvitationsClient mInvitationsClient = null;

    private AlertDialog mAlertDialog;

    // This is the current match we're in; null if not loaded
    public TurnBasedMatch mMatch;

    // This is the current match board_data after being unpersisted.
    // Do not retain references to match board_data once you have
    // taken an action on the match, such as takeTurn()
    public DamiTurn mTurnData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Create the Google API Client with access to Games
        // Create the client used to sign in.
        mGoogleSignInClient = GoogleSignIn.getClient(this, GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);


        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        game = new Dami(this);
        initialize(game, config);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (Dami.stage_type != null) {
                switch (Dami.stage_type) {
                    case menu:
                        // offer exit options
                        break;
                    case match:
                        game.setScreen(Dami.menuScreen);
                        break;
                    case store:
                        game.setScreen(Dami.menuScreen);
                        break;
                    default:
                        break;
                }
            }
        }
        return false;
    }

    @Override
    public void googlePlayGamesSignIn() {
        startSignInIntent();
    }

    // Create a one-on-one automatch game.
    public void onQuickMatchClicked(View view) {

        Bundle autoMatchCriteria = RoomConfig.createAutoMatchCriteria(1, 1, 0);

        TurnBasedMatchConfig turnBasedMatchConfig = TurnBasedMatchConfig.builder()
                .setAutoMatchCriteria(autoMatchCriteria).build();

//        showSpinner();

        // Start the match
        mTurnBasedMultiplayerClient.createMatch(turnBasedMatchConfig)
                .addOnSuccessListener(new OnSuccessListener<TurnBasedMatch>() {
                    @Override
                    public void onSuccess(TurnBasedMatch turnBasedMatch) {
//                        onInitiateMatch(turnBasedMatch);
                    }
                })
                .addOnFailureListener(createFailureListener("There was a problem creating a match!"));
    }

    // Open the create-game UI. You will get back an onActivityResult
    // and figure out what to do.
    @Override
    public void showSelectOpponent() {
        mTurnBasedMultiplayerClient.getSelectOpponentsIntent(1, 1, true)
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        new_match_intent = intent;
                        startActivityForResult(intent, RC_SELECT_PLAYERS);
                    }
                })
                .addOnFailureListener(createFailureListener(
                        getString(R.string.error_get_select_opponents)));
    }

    private void startSignInIntent() {
        startActivityForResult(mGoogleSignInClient.getSignInIntent(), RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {

        if (requestCode == RC_SIGN_IN) {

            Gdx.app.log(TAG, "signed in");

            Task<GoogleSignInAccount> task =
                    GoogleSignIn.getSignedInAccountFromIntent(intent);

            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                onConnected(account);
            } catch (ApiException apiException) {
                String message = apiException.getMessage();
                if (message == null || message.isEmpty()) {
                    message = getString(R.string.signin_other_error);
                }

                onDisconnected();

                new AlertDialog.Builder(this)
                        .setMessage(message)
                        .setNeutralButton(android.R.string.ok, null)
                        .show();
            }
        } else if (requestCode == RC_LOOK_AT_MATCHES) {
            // Returning from the 'Select Match' dialog

            if (resultCode != Activity.RESULT_OK) {
//              user returned from the 'Select Match' dialog so do nothing
                return;
            }

            TurnBasedMatch match = intent.getParcelableExtra(Multiplayer.EXTRA_TURN_BASED_MATCH);

//          update the match that was selected. loading...
            MainMenuScreen.addSpinner();
            updateMatch(match);

        } else if (requestCode == RC_SELECT_PLAYERS) {

            if (resultCode != Activity.RESULT_OK) {
//              user returned from the 'Select Players' dialog
                return;
            }


//       start a new match. loading...
            MainMenuScreen.addSpinner();
            // get the invitee list
            ArrayList<String> invitees = intent
                    .getStringArrayListExtra(Games.EXTRA_PLAYER_IDS);

            TurnBasedMatchConfig tbmc = TurnBasedMatchConfig.builder()
                    .addInvitedPlayers(invitees)
                    .setAutoMatchCriteria(null).build();

            // Start the match
            mTurnBasedMultiplayerClient.createMatch(tbmc)
                    .addOnSuccessListener(new OnSuccessListener<TurnBasedMatch>() {
                        @Override
                        public void onSuccess(TurnBasedMatch turnBasedMatch) {
                            startMatch(turnBasedMatch);
                        }
                    })
                    .addOnFailureListener(createFailureListener("There was a problem creating a match!"));
//            a loading icon can be shown here
        }
    }

    @Override
    public void initiateRematch() {
        startActivityForResult(new_match_intent, RC_SELECT_PLAYERS);
    }

    // This is the main function that gets called when players choose a match
    // from the inbox, or else create a match and want to start it.
    public void updateMatch(TurnBasedMatch match) {
        Log.d(TAG, "update match called");
        mMatch = match;

        int status = match.getStatus();
        int turnStatus = match.getTurnStatus();

        switch (status) {
            case TurnBasedMatch.MATCH_STATUS_CANCELED:
                showWarning("Canceled!", "This game was canceled!");
                return;
            case TurnBasedMatch.MATCH_STATUS_EXPIRED:
                showWarning("Expired!", "This game is expired.  So sad!");
                return;
            case TurnBasedMatch.MATCH_STATUS_AUTO_MATCHING:
                showWarning("Waiting for auto-match...",
                        "We're still waiting for an automatch partner.");
                return;
            case TurnBasedMatch.MATCH_STATUS_COMPLETE:
                if (turnStatus == TurnBasedMatch.MATCH_TURN_STATUS_COMPLETE) {
                    showWarning("Complete!",
                            "This game is over; someone finished it, and so did you!  " +
                                    "There is nothing to be done.");
                    break;
                }

                // Note that in this state, you must still call "Finish" yourself,
                // so we allow this to continue.
                showWarning("Complete!",
                        "This game is over; someone finished it!  You can only finish it now.");
        }

        // OK, it's active. show the match
        setGamePlayUI(mMatch);
    }

    private void setGamePlayUI(TurnBasedMatch mMatch) {
        mTurnData = DamiTurn.unpersist(mMatch.getData());
        game.setScreen(MainMenuScreen.onlineGameScreen);

        if (mTurnData != null) {
//          is it a rematch?
            if (mTurnData.board_data.equals("")) {
                mTurnData = new DamiTurn();
//              first board with no movement
                mTurnData.board_data = Utils.json_instance.toJson(Utils.getDefaultBoard());
//        initiating player is white
                mTurnData.white_owner_id = mPlayerId;
//        leave black null. will differentiate using the owner of white
                mTurnData.current_turn = "";
//                initiate empty moves array for new game
                mTurnData.current_moves = OnlineUserData.getEmptyMovesString();

//                initiate default board items for a rematch
                BoardItems boardItems = Utils.boardItems;


                CellEntry[][] cellEntries = Utils.getBoardFromJson(mTurnData.board_data);
                com.kente_factory.dami.enumerations.Player turn = com.kente_factory.dami.enumerations.Player.white;
                MainMenuScreen.onlineGameScreen.initialize(cellEntries, turn, Utils.getMovesFromJson(mTurnData.current_moves), mMatch.getMatchId(), boardItems);
            } else {
                CellEntry[][] cellEntries = Utils.getBoardFromJson(mTurnData.board_data);
                com.kente_factory.dami.enumerations.Player turn = Utils.getPlayerFromString(mTurnData.current_turn);
                ArrayList<Move> moves = Utils.getMovesFromJson(mTurnData.current_moves);

//                turn this on to update all pieces each time
//                mTurnData.white_shape = LocalUserData.pieceShape.toString();
//                mTurnData.white_crown = LocalUserData.crownType.toString();
//                mTurnData.black_shape = LocalUserData.pieceShape.toString();
//                mTurnData.black_crown = LocalUserData.crownType.toString();

                Log.d(TAG, mTurnData.white_owner_id + " is the white id");
                Log.d(TAG, mTurnData.black_owner_id + " is the black id");

//        mPlayerId is collected in onConnected method so if it is not equal to white_owner_id...
//        then this is the black player
                if (!isWhiteOwner()) {
                    mTurnData.black_owner_id = mPlayerId;
                }


                Log.d(TAG, mTurnData.white_owner_id + " is the white id after");
                Log.d(TAG, mTurnData.black_owner_id + " is the black id after");


                if (mMatch.getStatus() == TurnBasedMatch.MATCH_TURN_STATUS_MY_TURN
                        && turn.equals(com.kente_factory.dami.enumerations.Player.black)
                        && mTurnData.black_owner_id.equals(mPlayerId)) {
                    mTurnData.black_shape = LocalUserData.pieceShape.toString();
                    mTurnData.black_crown = LocalUserData.crownType.toString();
                }

                BoardItems boardItems = new BoardItems(mTurnData.white_shape, mTurnData.black_shape, mTurnData.white_crown, mTurnData.black_crown);

                Log.d("DAMITURN", turn + "");
                MainMenuScreen.onlineGameScreen.initialize(cellEntries, turn, moves, mMatch.getMatchId(), boardItems);
            }
        }
    }

    // startMatch() happens in response to the createTurnBasedMatch()
    // above. This is only called on success, so we should have a
    // valid match object. We're taking this opportunity to setup the
    // game, saving our initial state. Calling takeTurn() will
    // callback to OnTurnBasedMatchUpdated(), which will show the game UI.
    public void startMatch(TurnBasedMatch match) {
        Log.d(TAG, "start match called");
        mTurnData = new DamiTurn();
        // first board with no movement
        mTurnData.board_data = Utils.json_instance.toJson(Utils.getDefaultBoard());
        Log.d(TAG, "initiating with this data: " + Arrays.deepToString(Utils.getDefaultBoard()));
//        initiating player is white
        mTurnData.white_owner_id = mPlayerId;
//        leave black null. will differentiate using the owner of white
        mTurnData.current_turn = "white";
//                initiate empty moves array for new game
        mTurnData.current_moves = OnlineUserData.getEmptyMovesString();


//        since you are initializing, you are white and your items get to go in it
        BoardItems boardItems = new BoardItems();
        boardItems.white_shape = LocalUserData.pieceShape.toString();
        boardItems.white_crown = LocalUserData.crownType.toString();

        mTurnData.white_shape = boardItems.white_shape;
        mTurnData.white_crown = boardItems.white_crown;

        Log.d(TAG, "before Board items: " + mTurnData.black_shape + ", " + mTurnData.black_crown);
        mMatch = match;


        String myParticipantId = mMatch.getParticipantId(mPlayerId);

        mTurnBasedMultiplayerClient.takeTurn(match.getMatchId(),
                mTurnData.persist(), myParticipantId)
                .addOnSuccessListener(new OnSuccessListener<TurnBasedMatch>() {
                    @Override
                    public void onSuccess(TurnBasedMatch turnBasedMatch) {
                        updateMatch(turnBasedMatch);
                    }
                })
                .addOnFailureListener(createFailureListener("There was a problem taking a turn!"));

    }

    private void onConnected(GoogleSignInAccount googleSignInAccount) {
        Gdx.app.log(TAG, "onConnected(): connected to Google APIs");

        mTurnBasedMultiplayerClient = Games.getTurnBasedMultiplayerClient(this, googleSignInAccount);
        mInvitationsClient = Games.getInvitationsClient(this, googleSignInAccount);

        Games.getPlayersClient(this, googleSignInAccount)
                .getCurrentPlayer()
                .addOnSuccessListener(
                        new OnSuccessListener<Player>() {
                            @Override
                            public void onSuccess(Player player) {
                                mDisplayName = player.getDisplayName();
                                mPlayerId = player.getPlayerId();

                            }
                        }
                )
                .addOnFailureListener(createFailureListener("There was a problem getting the player!"));

        Log.d(TAG, "onConnected(): Connection successful");

        // Retrieve the TurnBasedMatch from the connectionHint
        GamesClient gamesClient = Games.getGamesClient(this, googleSignInAccount);
        gamesClient.getActivationHint()
                .addOnSuccessListener(new OnSuccessListener<Bundle>() {
                    @Override
                    public void onSuccess(Bundle hint) {

                    }
                })
                .addOnFailureListener(createFailureListener(
                        "There was a problem getting the activation hint!"));

        if (Dami.menuScreen != null) {
            Dami.menuScreen.updateButtonsOnSignInStateChanged(isSignedIn());
        }

        // As a demonstration, we are registering this activity as a handler for
        // invitation and match events.

        // This is *NOT* required; if you do not register a handler for
        // invitation events, you will get standard notifications instead.
        // Standard notifications may be preferable behavior in many cases.
        mInvitationsClient.registerInvitationCallback(mInvitationCallback);

        // Likewise, we are registering the optional MatchUpdateListener, which
        // will replace notifications you would get otherwise. You do *NOT* have
        // to register a MatchUpdateListener.
        mTurnBasedMultiplayerClient.registerTurnBasedMatchUpdateCallback(mMatchUpdateCallback);
    }

    private TurnBasedMatchUpdateCallback mMatchUpdateCallback = new TurnBasedMatchUpdateCallback() {
        @Override
        public void onTurnBasedMatchReceived(@NonNull TurnBasedMatch turnBasedMatch) {
            Log.d(TAG, "A match was updated");
            Log.d(TAG, turnBasedMatch.getStatus() + "");

            Log.d(TAG, "" + turnBasedMatch.getMatchId());
//          get new data from the match
            DamiTurn opponentsData = DamiTurn.unpersist(turnBasedMatch.getData());
            Log.d(TAG, "Board items(black): " + opponentsData.black_shape + ", " + opponentsData.black_crown);
            Log.d(TAG, "Board items(white): " + opponentsData.white_shape + ", " + opponentsData.white_crown);
//            parse it...
            CellEntry[][] cellEntries = Utils.getBoardFromJson(opponentsData.board_data);
            com.kente_factory.dami.enumerations.Player turn = Utils.getPlayerFromString(opponentsData.current_turn);
            ArrayList<Move> moves = Utils.getMovesFromJson(opponentsData.current_moves);

            BoardItems boardItems = new BoardItems(opponentsData.white_shape, opponentsData.black_shape,
                    opponentsData.white_crown, opponentsData.black_crown);

//            callback should only cause reinitializing if it is not the other guy's turn. to fix replaying my turn when
//            opponent accepts my callback.
            if (isMyTurn(turn)) {
                Toast.makeText(AndroidLauncher.this, "It is your turn!", Toast.LENGTH_LONG).show();
//            ...and update the match
                if (MainMenuScreen.onlineGameScreen.getCurrentMatchId() != null) {
                    if (MainMenuScreen.onlineGameScreen.getCurrentMatchId().equals(turnBasedMatch.getMatchId())) {
                        MainMenuScreen.onlineGameScreen.initialize(cellEntries, turn, moves, turnBasedMatch.getMatchId(), boardItems);
                    }
                }
            }
        }

        @Override
        public void onTurnBasedMatchRemoved(@NonNull String matchId) {
            Toast.makeText(AndroidLauncher.this, "A match was removed.", Toast.LENGTH_SHORT).show();
        }
    };


    // This is a helper function that will do all the setup to create a simple failure message.
    // Add it to any task and in the case of an failure, it will report the string in an alert
    // dialog.
    private OnFailureListener createFailureListener(final String string) {
        return new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                handleException(e, string);
            }
        };
    }


    /**
     * Since a lot of the operations use tasks, we can use a common handler for whenever one fails.
     *
     * @param exception The exception to evaluate.  Will try to display a more descriptive reason for
     *                  the exception.
     * @param details   Will display alongside the exception if you wish to provide more details for
     *                  why the exception happened
     */
    private void handleException(Exception exception, String details) {
        int status = 0;

        if (exception instanceof TurnBasedMultiplayerClient.MatchOutOfDateApiException) {
            TurnBasedMultiplayerClient.MatchOutOfDateApiException matchOutOfDateApiException =
                    (TurnBasedMultiplayerClient.MatchOutOfDateApiException) exception;

            new AlertDialog.Builder(this)
                    .setMessage("Match was out of date, updating with latest match board_data...")
                    .setNeutralButton(android.R.string.ok, null)
                    .show();

            TurnBasedMatch match = matchOutOfDateApiException.getMatch();
//            updateMatch(match);

            return;
        }

        if (exception instanceof ApiException) {
            ApiException apiException = (ApiException) exception;
            status = apiException.getStatusCode();
        }

        if (!checkStatusCode(status)) {
            return;
        }

        String message = getString(R.string.status_exception_error, details, status, exception);

        new AlertDialog.Builder(this)
                .setMessage(message)
                .setNeutralButton(android.R.string.ok, null)
                .show();
    }

    // Displays your inbox. You will get back onActivityResult where
    // you will need to figure out what you clicked on.
    @Override
    public void checkGames() {
        mTurnBasedMultiplayerClient.getInboxIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_LOOK_AT_MATCHES);
                    }
                })
                .addOnFailureListener(createFailureListener(getString(R.string.error_get_inbox_intent)));
    }

    @Override
    public void updateOnlineMatch(final boolean finish) {
        mTurnData.board_data = OnlineUserData.board_data_json;
        mTurnData.current_turn = OnlineUserData.current_turn_string;
        mTurnData.current_moves = OnlineUserData.current_moves_string;

        Log.d(TAG, "current state of affairs " + mTurnData.current_turn + ", " + isWhiteOwner());

        String nextParticipantId = getNextParticipantId();


//        take a turn
        mTurnBasedMultiplayerClient.takeTurn(mMatch.getMatchId(),
                mTurnData.persist(), nextParticipantId)
                .addOnSuccessListener(new OnSuccessListener<TurnBasedMatch>() {
                    @Override
                    public void onSuccess(TurnBasedMatch turnBasedMatch) {
//                        looks like something sensible could be done here
                    }
                })
                .addOnFailureListener(createFailureListener("There was a problem taking a turn!"));
    }

    @Override
    public void endOnlineMatch() {
        Log.d(TAG, mMatch.getStatus() + "...is the status");
        if(mMatch.getStatus() != TurnBasedMatch.MATCH_STATUS_COMPLETE) {
            mTurnBasedMultiplayerClient.finishMatch(mMatch.getMatchId()).addOnFailureListener(createFailureListener(
                    "There was a problem finishing the match!")).addOnSuccessListener(new OnSuccessListener<TurnBasedMatch>() {
                @Override
                public void onSuccess(TurnBasedMatch turnBasedMatch) {
                    AndroidLauncher.this.mMatch = turnBasedMatch;
                }
            });
        }
    }

    @Override
    public boolean isGooglePlayGamesSignedIn() {
        return isSignedIn();
    }

    @Override
    public boolean isWhiteOwner() {
        return mTurnData != null && mTurnData.white_owner_id.equals(mPlayerId);
    }

    /**
     * Get the next participant. In this function, we assume that we are
     * round-robin, with all known players going before all automatch players.
     * This is not a requirement; players can go in any order. However, you can
     * take turns in any order.
     *
     * @return participantId of next player, or null if automatching
     */
    public String getNextParticipantId() {

        String myParticipantId = mMatch.getParticipantId(mPlayerId);

        ArrayList<String> participantIds = mMatch.getParticipantIds();

        int desiredIndex = -1;

        for (int i = 0; i < participantIds.size(); i++) {
            if (participantIds.get(i).equals(myParticipantId)) {
                desiredIndex = i + 1;
            }
        }

        if (desiredIndex < participantIds.size()) {
            return participantIds.get(desiredIndex);
        }

        if (mMatch.getAvailableAutoMatchSlots() <= 0) {
            // You've run out of automatch slots, so we start over.
            return participantIds.get(0);
        } else {
            // You have not yet fully automatched, so null will find a new
            // person to play against.
            return null;
        }
    }

    // Returns false if something went wrong, probably. This should handle
    // more cases, and probably report more accurate results.
    private boolean checkStatusCode(int statusCode) {
        switch (statusCode) {
            case GamesCallbackStatusCodes.OK:
                return true;

            case GamesClientStatusCodes.MULTIPLAYER_ERROR_NOT_TRUSTED_TESTER:
                showErrorMessage(R.string.status_multiplayer_error_not_trusted_tester);
                break;
            case GamesClientStatusCodes.MATCH_ERROR_ALREADY_REMATCHED:
                showErrorMessage(R.string.match_error_already_rematched);
                break;
            case GamesClientStatusCodes.NETWORK_ERROR_OPERATION_FAILED:
                showErrorMessage(R.string.network_error_operation_failed);
                break;
            case GamesClientStatusCodes.INTERNAL_ERROR:
                showErrorMessage(R.string.internal_error);
                break;
            case GamesClientStatusCodes.MATCH_ERROR_INACTIVE_MATCH:
                showErrorMessage(R.string.match_error_inactive_match);
                break;
            case GamesClientStatusCodes.MATCH_ERROR_LOCALLY_MODIFIED:
                showErrorMessage(R.string.match_error_locally_modified);
                break;
            default:
                showErrorMessage(R.string.unexpected_status);
                Log.d(TAG, "Did not have warning or string to deal with: "
                        + statusCode);
        }

        return false;
    }

    public void showErrorMessage(int stringId) {
        showWarning("Warning", getResources().getString(stringId));
    }

    // Generic warning/info dialog
    public void showWarning(String title, String message) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(title).setMessage(message);

        // set dialog message
        alertDialogBuilder.setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, close
                        // current activity
                    }
                });

        // create alert dialog
        mAlertDialog = alertDialogBuilder.create();

        // show it
        mAlertDialog.show();
    }

    @Override
    public void googlePlayGamesSignOut() {

        mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            Log.d(TAG, "signOut(): success");
                        } else {
                            handleException(task.getException(), "signOut() failed!");
                        }

                        onDisconnected();
                    }
                });
    }

    private void onDisconnected() {

        mTurnBasedMultiplayerClient = null;
        mInvitationsClient = null;

        Gdx.app.log(TAG, "google play disconnected");

//        indicate disconnected
        if (Dami.menuScreen != null) {
            Dami.menuScreen.updateButtonsOnSignInStateChanged(isSignedIn());
        }
    }

    @Override
    protected void onResume() {

//        handle the callbacks now that you are on the screen
        if (mInvitationsClient != null) {
            mInvitationsClient.registerInvitationCallback(mInvitationCallback);
        }
        if (mTurnBasedMultiplayerClient != null) {
            mTurnBasedMultiplayerClient.registerTurnBasedMatchUpdateCallback(mMatchUpdateCallback);
        }

        super.onResume();
        Gdx.app.log("DAMILIFECYCLE", "onResume");

        // Since the state of the signed in user can change when the activity is not active
        // it is recommended to try and sign in silently from when the app resumes.
        signInSilently();
    }

    private InvitationCallback mInvitationCallback = new InvitationCallback() {
        // Handle notification events.
        @Override
        public void onInvitationReceived(@NonNull Invitation invitation) {
            Toast.makeText(
                    AndroidLauncher.this,
                    invitation.getInviter().getDisplayName() +
                            " has challenged you to a match!", Toast.LENGTH_LONG)
                    .show();
        }

        @Override
        public void onInvitationRemoved(@NonNull String invitationId) {
            Toast.makeText(AndroidLauncher.this, "An invitation was removed.", Toast.LENGTH_SHORT)
                    .show();
        }
    };

    /**
     * Try to sign in without displaying dialogs to the user.
     * <p>
     * If the user has already signed in previously, it will not show dialog.
     */
    public void signInSilently() {
        Log.d(TAG, "signInSilently()");

        mGoogleSignInClient.silentSignIn().addOnCompleteListener(this,
                new OnCompleteListener<GoogleSignInAccount>() {
                    @Override
                    public void onComplete(@NonNull Task<GoogleSignInAccount> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInSilently(): success");
                            onConnected(task.getResult());
                        } else {
                            Log.d(TAG, "signInSilently(): failure", task.getException());
                            onDisconnected();
                        }
                    }
                });
    }

    @Override
    protected void onPause() {
//        unregister the callbacks so that the notifications can come through
        if (mInvitationsClient != null) {
            mInvitationsClient.unregisterInvitationCallback(mInvitationCallback);
        }
        if (mTurnBasedMultiplayerClient != null) {
            mTurnBasedMultiplayerClient.unregisterTurnBasedMatchUpdateCallback(mMatchUpdateCallback);
        }

        Gdx.app.log("DAMILIFECYCLE", "onPause");

        super.onPause();
    }

    private boolean isSignedIn() {
        return mTurnBasedMultiplayerClient != null;
    }

    private boolean isMyTurn(com.kente_factory.dami.enumerations.Player turn) {
        if (isWhiteOwner() && turn.equals(com.kente_factory.dami.enumerations.Player.white)) {
            return true;
        }

        if (!isWhiteOwner() && turn.equals(com.kente_factory.dami.enumerations.Player.black)) {
            return true;
        }
        return false;
    }
}
