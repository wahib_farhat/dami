package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.kente_factory.dami.enumerations.MatchType;
import com.kente_factory.dami.logic.GameScreen;

public class QuitGameMessage extends Group {

    private com.kente_factory.dami.enumerations.MatchType matchType;
    private GameScreen gameScreen;

    public QuitGameMessage(final Dami game, com.kente_factory.dami.enumerations.MatchType matchType, GameScreen gameScreen) {
        float x = 57f / 2;
        float y = 478f / 2;

        this.matchType = matchType;
        this.gameScreen = gameScreen;

        Pixmap dark_bg = new Pixmap(1080, 1920, Pixmap.Format.RGBA8888);
        dark_bg.setColor(new Color(0, 0, 0, 0.5f));
        dark_bg.fill();

        Pixmap background = new Pixmap(965, 965, Pixmap.Format.RGBA8888);
        background.setColor(new Color(196f / 225, 154f / 225, 108f / 225, 1));
        background.fill();

        Entity black_bg = new Entity(new Texture(dark_bg), 0, 0);

        Entity bg = new Entity(new Texture(background), x, y);
        Button yes_button = new Button(Assets.yes_buttonTexture, x + 120, y + 100, new ButtonAction() {
            @Override
            public void onClick() {
//                Utils.saveGame();
                QuitGameMessage.this.remove();
                game.setScreen(Dami.menuScreen);
            }
        });

        Button no_button = new Button(Assets.no_buttonTexture, x + 270, y + 100, new ButtonAction() {
            @Override
            public void onClick() {
                QuitGameMessage.this.remove();
                Gdx.input.setInputProcessor(new InputMultiplexer(QuitGameMessage.this.gameScreen.stage, QuitGameMessage.this.gameScreen.HUD));
            }
        });

        this.addActor(black_bg);
        this.addActor(bg);
        this.addActor(yes_button);
        this.addActor(no_button);

        Texture messageTexture = null;

        if(this.matchType.equals(MatchType.local)) {
            messageTexture = Assets.quit_game_messageTexture;
        } else {
            messageTexture = Assets.exit_game_messageTexture;
        }

        Entity messageEntity = new Entity(messageTexture, x + 50f, y + 270f);
        this.addActor(messageEntity);
    }
}
