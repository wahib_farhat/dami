package com.kente_factory.dami;

import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.logic.Move;

import java.util.ArrayList;

/**
 * Wahib
 *
 * Misnomer from myself. UserData does not imply local persistent storage but in fact,
 * hot variables to be used in translating things from the DamiTurn into data objects that can be used by
 * GameScreen and Board classes
 */
public class OnlineUserData {
    public static String board_data_json;
    public static CellEntry[][] cellEntries;
    public static String owned_player_string;
    public static String current_turn_string;
    public static String current_moves_string;

    public static void initialize() {
        // put in the stored board or give him the default board?d
        board_data_json = null;

//        these can be null as they will be decided in the beginning depending on the content of mTurnData
        owned_player_string = null;
        current_turn_string = null;
        current_moves_string = null;
    }

    public static void updateOnlineBoardLocal(String new_board) {
        board_data_json = new_board;
    }

    public static void updateOnlineBoardActual(CellEntry[][] cellEntries) {
        OnlineUserData.cellEntries = cellEntries;
    }

    public static void updateOnlineCurrentPlayerLocal(String player) {
        current_turn_string = player;
    }

    public static void updateOnlineCurrentMovesLocal(String moves) {
        current_moves_string = moves;
    }

    public static String getEmptyMovesString() {

        return Utils.json_instance.toJson(new ArrayList<Move>());
    }
}
