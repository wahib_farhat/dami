package com.kente_factory.dami.logic;

// board properties that have to do with each player's items owned

public class BoardItems {

    public String white_shape = "circle";
    public String black_shape = "circle";
    public String white_crown = "normal";
    public String black_crown = "normal";

//    unused constructor and allow user to set the fields as he/she likes
    public BoardItems() {

    }

//    or utility constructor to set things the way you like all at once
    public BoardItems(String white_shape, String black_shape, String white_crown, String black_crown) {
        this.white_shape = white_shape;
        this.black_shape = black_shape;
        this.white_crown = white_crown;
        this.black_crown = black_crown;
    }
}
