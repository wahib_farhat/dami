package com.kente_factory.dami.logic;

import com.badlogic.gdx.Gdx;
import com.kente_factory.dami.enumerations.MoveDir;

import java.util.ArrayList;

/**
 * Created by wahib on 3/30/2018.
 */

public class Tests {

    public static void testBoxesInDiagonal() {
        ArrayList<int[]> boxes1 = Board.getBoxesInDiagonal(0, 0, 8, 8, MoveDir.forwardRight);
        ArrayList<int[]> boxes2 = Board.getBoxesInDiagonal(0, 9, 7, 2, MoveDir.forwardLeft);
        ArrayList<int[]> boxes3 = Board.getBoxesInDiagonal(9, 0, 2, 7, MoveDir.backwardRight);
        ArrayList<int[]> boxes4 = Board.getBoxesInDiagonal(9, 9, 2, 2, MoveDir.backwardLeft);


        Gdx.app.log("dami_diagnostics", "/n/n top right");
        for (int[] box : boxes4) {
            Gdx.app.log("dami_diagnostics", box[0] + " " + box[1]);
            Gdx.app.log("dami_diagnostics", "" + Board.onSameDiagonal(9, 9, box[0], box[1]));
        }
        Gdx.app.log("dami_diagnostics", "/n/n bottom left");
        for (int[] box : boxes1) {
            Gdx.app.log("dami_diagnostics", box[0] + " " + box[1]);
            Gdx.app.log("dami_diagnostics", "" + Board.onSameDiagonal(0, 0, box[0], box[1]));
        }

        Gdx.app.log("dami_diagnostics", "/n/n bottom right");
        for (int[] box : boxes2) {
            Gdx.app.log("dami_diagnostics", box[0] + " " + box[1]);
            Gdx.app.log("dami_diagnostics", "" + Board.onSameDiagonal(0, 9, box[0], box[1]));
        }

        Gdx.app.log("dami_diagnostics", "/n/n top left");
        for (int[] box : boxes3) {
            Gdx.app.log("dami_diagnostics", box[0] + " " + box[1]);
            Gdx.app.log("dami_diagnostics", "" + Board.onSameDiagonal(9, 0, box[0], box[1]));
        }
    }
}
