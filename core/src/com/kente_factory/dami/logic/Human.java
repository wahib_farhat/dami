/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kente_factory.dami.logic;

import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.enumerations.Player;

import java.util.Vector;

/**
 * @author apurv
 */
public class Human {

    public static void makeNextWhiteMoves() {
        boolean incorrectOption = true;
        while (incorrectOption) {
            Move move = UserInteractions.getNextMove(com.kente_factory.dami.enumerations.Player.white);
            if (CheckValidMoveForWhiteHuman(move.initialRow, move.initialCol,
                    move.finalRow, move.finalCol)) {
                incorrectOption = false;
            }
        }

    }


    public static void makeNextBlackMoves() {
        boolean incorrectOption = true;
        while (incorrectOption) {
            Move move = UserInteractions.getNextMove(Player.black);
            if (CheckValidMoveForBlackHuman(move.initialRow, move.initialCol,
                    move.finalRow, move.finalCol)) {
                incorrectOption = false;
            }
        }

    }


    public static boolean CheckValidMoveForWhiteHuman(int r1, int c1, int r2, int c2) {
        // Select Right Piece and Right Move
        if (
                GameScreen.board.cell_entries[r1][c1].equals(com.kente_factory.dami.enumerations.CellEntry.inValid) ||
                        !(
                                GameScreen.board.cell_entries[r1][c1].equals(com.kente_factory.dami.enumerations.CellEntry.white) ||
                                        GameScreen.board.cell_entries[r1][c1].equals(com.kente_factory.dami.enumerations.CellEntry.whiteKing)
                        )
                        || !GameScreen.board.cell_entries[r2][c2].equals(com.kente_factory.dami.enumerations.CellEntry.empty)
                ) {
            UserInteractions.PrintSeparator('-');
            System.out.println("Check !!! Black/Invalid Piece Selected or Invalid Move..... Try Again.");
            UserInteractions.PrintSeparator('-');
            return false;
        }

        // Caution: Calculate forced moves at (r1,c1)-------------------------------------------
        Vector<Move> forcedMovesAtR1C1 = White.ObtainForcedMovesForWhite(r1, c1, GameScreen.board);

        // If Forced Move exists at (r1, c1)
        if (!forcedMovesAtR1C1.isEmpty()) {
            Move move = new Move(r1, c1, r2, c2);

            // Caution: if the current move is a forced move -------------------------------------------
            if (move.ExistsInVector(forcedMovesAtR1C1)) {
                // then it is a valid move
                return true;

            } else {
                System.out.println("Check!!!Wrong Move....Try Again.");
                System.out.println("You have the following moves at: (r1: " + r1 + ", c1: " + c1 + ")");
                return false;
            }
        }

        // If no forced move exists at (r1,c1)
        if (forcedMovesAtR1C1.isEmpty()) {
            // Caution: Calculate all forced moves for white at this state of the board-------------------
            Vector<Move> forcedMoves = White.CalculateAllForcedMovesForWhite(GameScreen.board);

            // No forced move exists at this state of the board for white
            if (forcedMoves.isEmpty()) {
                // Forward Move
                if (r2 - r1 == 1 && Math.abs(c2 - c1) == 1) {
//                    GameScreen.board.MakeMove(r1, c1, r2, c2);
                    return true;
                }

                // Backward Move For WhiteKing
                else if (GameScreen.board.cell_entries[r1][c1].equals(com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
//                    if (r2 - r1 == -1 && Math.abs(c2 - c1) == 1) {
////                        GameScreen.board.MakeMove(r1, c1, r2, c2);
//                        return true;
//                    }
//                    // same diagonal, implies length between their x and y axes are equal
//                    if(Math.abs(r2 - r1) == Math.abs(c2 - c1)){
//                        return true;
//                    }
                    return GameScreen.board.whiteKingCanMakeMove(r1, c1, r2, c2);
                } else {
                    UserInteractions.PrintSeparator('-');
                    System.out.println("Check!!!Only Unit Step Move Allowed.......Try Again.\n");
                    UserInteractions.PrintSeparator('-');
                    return false;
                }
            } else {
                UserInteractions.PrintSeparator('-');

                System.out.println("Forced Move exists!!!!!!!!!!!");
                System.out.println("You have the following options.");
                for (int i = 0; i < forcedMoves.size(); i++) {
                    System.out.print((i + 1) + ". ");
                    System.out.print("(r1: " + forcedMoves.elementAt(i).initialRow + ", ");
                    System.out.print("c1: " + forcedMoves.elementAt(i).initialCol + ")");
                    System.out.print("------> (r2: " + forcedMoves.elementAt(i).finalRow + ", ");
                    System.out.println("c2: " + forcedMoves.elementAt(i).finalCol + ")");
                }

                UserInteractions.PrintSeparator('-');
                return false;
            }
        }

        return false;
    }


    public static boolean CheckValidMoveForBlackHuman(int r1, int c1, int r2, int c2) {
        // Select Right Piece and Right Move
        if (
                GameScreen.board.cell_entries[r1][c1].equals(com.kente_factory.dami.enumerations.CellEntry.inValid) ||
                        !(
                                GameScreen.board.cell_entries[r1][c1].equals(com.kente_factory.dami.enumerations.CellEntry.black) ||
                                        GameScreen.board.cell_entries[r1][c1].equals(com.kente_factory.dami.enumerations.CellEntry.blackKing)
                        )
                        || !GameScreen.board.cell_entries[r2][c2].equals(com.kente_factory.dami.enumerations.CellEntry.empty)
                ) {
            UserInteractions.PrintSeparator('-');
            System.out.println("Check !!! White/Invalid Piece Selected or Invalid Move..... Try Again.");
            UserInteractions.PrintSeparator('-');
            return false;
        }

        // Caution: Calculate forced moves at (r1,c1)-------------------------------------------
        Vector<Move> forcedMovesAtR1C1 = Black.ObtainForcedMovesForBlack(r1, c1, GameScreen.board);

        // If Forced Move exists at (r1, c1)
        if (!forcedMovesAtR1C1.isEmpty()) {
            Move move = new Move(r1, c1, r2, c2);

            // Caution: if the current move is a forced move -------------------------------------------
            if (move.ExistsInVector(forcedMovesAtR1C1)) {
                // then it is a valid move
                return true;
            } else {

                System.out.println("Forced Move exists!!!!!!!!!!!");
                System.out.println("You have the following moves at: (r1: " + r1 + ", c1: " + c1 + ")");

                UserInteractions.PrintSeparator('-');
                return false;
            }
        }

        // If no forced move exists at (r1,c1)
        if (forcedMovesAtR1C1.isEmpty()) {
            // Caution: Calculate all forced moves for black at this state of the board-------------------
            Vector<Move> forcedMoves = Black.CalculateAllForcedMovesForBlack(GameScreen.board);

            // No forced move exists at this state of the board for black
            if (forcedMoves.isEmpty()) {
                // Forward Move for Black
                if (r2 - r1 == -1 && Math.abs(c2 - c1) == 1) {
//                    GameScreen.board.MakeMove(r1, c1, r2, c2);
                    return true;
                }

                // Backward Move For BlackKing
                else if (GameScreen.board.cell_entries[r1][c1].equals(CellEntry.blackKing)) {
                    return GameScreen.board.blackKingCanMakeMove(r1, c1, r2, c2);
                } else {
                    UserInteractions.PrintSeparator('-');
                    System.out.println("Check!!!Only Unit Step Move Allowed.......Try Again.");
                    UserInteractions.PrintSeparator('-');
                    return false;
                }
            } else {
                UserInteractions.PrintSeparator('-');
                System.out.println("Forced Move exists!!!!!!!!!!!");
                System.out.println("You have the following options.");
                for (int i = 0; i < forcedMoves.size(); i++) {
                    System.out.print((i + 1) + ". ");
                    System.out.print("(r1: " + forcedMoves.elementAt(i).initialRow + ", ");
                    System.out.print("c1: " + forcedMoves.elementAt(i).initialCol + ")");
                    System.out.print("------> (r2: " + forcedMoves.elementAt(i).finalRow + ", ");
                    System.out.println("c2: " + forcedMoves.elementAt(i).finalCol + ")");
                }

                UserInteractions.PrintSeparator('-');
                return false;
            }
        }

        return false;
    }


}
