/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kente_factory.dami.logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kente_factory.dami.Assets;
import com.kente_factory.dami.BlackBox;
import com.kente_factory.dami.Entity;
import com.kente_factory.dami.Utils;
import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.enumerations.MoveDir;
import com.kente_factory.dami.enumerations.Player;
import com.kente_factory.dami.enumerations.store_item_types.CrownType;
import com.kente_factory.dami.enumerations.store_item_types.PieceShape;

import java.util.ArrayList;
import java.util.Vector;

/**
 * @author WAHIB
 */
public class Board {

    int blackPieces;
    int whitePieces;

    static final int rows = 10;
    static final int cols = 10;
    public CellEntry cell_entries[][];

    private GameScreen gameScreen;

    public Actor edge;

    public Board(GameScreen gameScreen, BoardItems boardItems) {
//        not needed as Utils tells number of pieces of each color by counting on the board
//        this.blackPieces = this.whitePieces = 20;
        this.gameScreen = gameScreen;

        this.cell_entries = new CellEntry[][]{
                {CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid},
                {CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white},
                {CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid},
                {CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white},
                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
                {CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid},
                {CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black},
                {CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid},
                {CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black},
        };

//        this.cell_entries = new CellEntry[][]{
//                {CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//        };

//        forward left tests
//        this.cell_entries = new CellEntry[][]{
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.whiteKing},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//        };


//        forward right tests
//        this.cell_entries = new CellEntry[][]{
//                {CellEntry.blackKing, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//        };


//        backward left tests
//        this.cell_entries = new CellEntry[][]{
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.blackKing, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//        };

//        backward right tests
//        this.cell_entries = new CellEntry[][]{
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
//                {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
//                {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.blackKing},
//        };
//        this caused you a big headache! don't do it again
//        this.cell_entries = Utils.getDefaultBoard();

        setBlackPieces(Utils.getBlackPiecesFromBoardData(this.cell_entries));
        setWhitePieces(Utils.getWhitePiecesFromBoardData(this.cell_entries));


//        add board visuals
        gameScreen.stage.addActor(new Entity(Assets.board_backTexture, 0, 0));
        edge = new Entity(Assets.board_edgeTexture, 0, 192.1455f);
        gameScreen.stage.addActor(edge);
        for (int i = 0; i < 10; i += 1) {
            for (int j = 0; j < 10; j += 1) {
                if ((i + j) % 2 == 0) {
                    gameScreen.stage.addActor(new BlackBox(gameScreen, i, j));
                }
            }
        }

        PieceShape white_pieceShape = PieceShape.valueOf(boardItems.white_shape);
        PieceShape black_pieceShape = PieceShape.valueOf(boardItems.black_shape);
        CrownType white_crownType = CrownType.valueOf(boardItems.white_crown);
        CrownType black_crownType = CrownType.valueOf(boardItems.black_crown);

        for (int i = 0; i < cell_entries.length; i++) {
            for (int j = 0; j < cell_entries[i].length; j++) {
                Gdx.app.log("cells", i + ", " + j);
                if (cell_entries[i][j].equals(CellEntry.white)) {
                    gameScreen.stage.addActor(new Piece(gameScreen, Player.white, j, i, white_pieceShape, white_crownType));
                }

                if (cell_entries[i][j].equals(CellEntry.black)) {
                    gameScreen.stage.addActor(new Piece(gameScreen, Player.black, j, i, black_pieceShape, black_crownType));
                }

                if (cell_entries[i][j].equals(CellEntry.whiteKing)) {
                    Piece piece = new Piece(gameScreen, Player.white, j, i, white_pieceShape, white_crownType);
                    piece.setKing(true);
                    gameScreen.stage.addActor(piece);
                }

                if (cell_entries[i][j].equals(CellEntry.blackKing)) {
                    Piece piece = new Piece(gameScreen, Player.black, j, i, black_pieceShape, black_crownType);
                    piece.setKing(true);
                    gameScreen.stage.addActor(piece);
                }
            }
        }
    }

    public Board(GameScreen gameScreen, CellEntry[][] cellEntries, BoardItems boardItems) {

        setBlackPieces(Utils.getBlackPiecesFromBoardData(cellEntries));
        setWhitePieces(Utils.getWhitePiecesFromBoardData(cellEntries));

        this.gameScreen = gameScreen;

        this.cell_entries = cellEntries;

//        add board visuals
        gameScreen.stage.addActor(new Entity(Assets.board_backTexture, 0, 0));
        edge = new Entity(Assets.board_edgeTexture, 0, 192.1455f);
        gameScreen.stage.addActor(edge);
        for (int i = 0; i < 10; i += 1) {
            for (int j = 0; j < 10; j += 1) {
                if ((i + j) % 2 == 0) {
                    gameScreen.stage.addActor(new BlackBox(gameScreen, i, j));
                }
            }
        }

        PieceShape white_pieceShape = PieceShape.valueOf(boardItems.white_shape);
        PieceShape black_pieceShape = PieceShape.valueOf(boardItems.black_shape);
        CrownType white_crownType = CrownType.valueOf(boardItems.white_crown);
        CrownType black_crownType = CrownType.valueOf(boardItems.black_crown);

        for (int i = 0; i < cell_entries.length; i++) {
            for (int j = 0; j < cell_entries[i].length; j++) {
                Gdx.app.log("cells", i + ", " + j);
                if (cell_entries[i][j].equals(CellEntry.white)) {
                    gameScreen.stage.addActor(new Piece(gameScreen, Player.white, j, i, white_pieceShape, white_crownType));
                }

                if (cell_entries[i][j].equals(CellEntry.black)) {
                    gameScreen.stage.addActor(new Piece(gameScreen, Player.black, j, i, black_pieceShape, black_crownType));
                }

                if (cell_entries[i][j].equals(CellEntry.whiteKing)) {
                    Piece piece = new Piece(gameScreen, Player.white, j, i, white_pieceShape, white_crownType);
                    piece.setKing(true);
                    gameScreen.stage.addActor(piece);
                }

                if (cell_entries[i][j].equals(CellEntry.blackKing)) {
                    Piece piece = new Piece(gameScreen, Player.black, j, i, black_pieceShape, black_crownType);
                    piece.setKing(true);
                    gameScreen.stage.addActor(piece);
                }
            }
        }
    }

    public CellEntry[][] getCell_entries() {
        return cell_entries;
    }

    public void setCell_entries(CellEntry[][] cell_entries) {
        this.cell_entries = cell_entries;
    }

    public int getBlackPieces() {
        return blackPieces;
    }

    public void setBlackPieces(int blackPieces) {
        this.blackPieces = blackPieces;
    }

    public int getWhitePieces() {
        return whitePieces;
    }

    public void setWhitePieces(int whitePieces) {
        this.whitePieces = whitePieces;
    }

    Board(CellEntry[][] board) {
        this.blackPieces = this.whitePieces = 20;

        this.cell_entries = new CellEntry[rows][cols];
        for (int i = 0; i < rows; i++) {
            System.arraycopy(board[i], 0, this.cell_entries[i], 0, cols);
        }
    }

    private void MakeMove(int r1, int c1, final int r2, final int c2, final boolean forced) {

        final Move move = new Move(r1, c1, r2, c2);

        if (gameScreen != null) {
            Gdx.app.log("randomLog", "black: " + this.blackPieces + ", white: " + this.whitePieces);
            Vector2 origin = getPieceCoordinates(r1, c1);
            final Vector2 destination = getPieceCoordinates(r2, c2);

            final Piece piece;


            piece = (Piece) gameScreen.stage.hit(origin.x, origin.y, false);


            if (piece != null) {
                Gdx.app.log("added Action", "again");
                piece.addAction(Actions.sequence(Actions.moveTo(destination.x, destination.y, 0.25f), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        piece.setColRow(c2, r2);

//                        check for king and if so replace texture
                        if (Board.this.cell_entries[r2][c2].equals(CellEntry.whiteKing) && r2 == rows - 1) {

                            piece.setKing(true);

                        } else if (Board.this.cell_entries[r2][c2].equals(CellEntry.blackKing) && r2 == 0) {
                            piece.setKing(true);
                        }

                        if (forced) {
//                          capture sound
                            piece.playCaptureSound();

                            // check for next move
                            if (canReleaseTurn(r2, c2, piece, move)) {
                                // tell game screen you are done
                                gameScreen.setPlaying(false);
                            } else {
                                // could not release for next turn, give it to computer
                                if (gameScreen.isComputerTurn()) {
                                    gameScreen.playAturn();
                                }
                                // or make same color touchable again for next move
                                gameScreen.updateAllTouchable(false);
                            }
                        } else {
//                            move sound
                            piece.playMoveSound();
                            // tell game screen you are done
                            gameScreen.setPlaying(false);
                        }
                    }
                })));
                if (GameScreen.selectIcon != null && GameScreen.selectIcon.getStage() != null) {
                    GameScreen.selectIcon.remove();
                }
            }
        }

        this.cell_entries[r2][c2] = this.cell_entries[r1][c1];
        this.cell_entries[r1][c1] = CellEntry.empty;

        // Promote To King
        if (this.cell_entries[r2][c2].equals(CellEntry.white) && r2 == rows - 1) {
            this.cell_entries[r2][c2] = CellEntry.whiteKing;

        } else if (this.cell_entries[r2][c2].equals(CellEntry.black) && r2 == 0) {
            this.cell_entries[r2][c2] = CellEntry.blackKing;
        }
    }

    private void MakeMoveAnimation(final Move move, final boolean forced, final ArrayList<Move> moves) {

        if (gameScreen != null) {
            Vector2 origin = getPieceCoordinates(move.initialRow, move.initialCol);
            final Vector2 destination = getPieceCoordinates(move.finalRow, move.finalCol);

            final Piece counter;


            counter = (Piece) gameScreen.stage.hit(origin.x, origin.y, false);


            if (counter != null) {
                counter.addAction(Actions.sequence(Actions.moveTo(destination.x, destination.y, 0.3f, Interpolation.circleOut), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        counter.setColRow(move.finalCol, move.finalRow);

//                        check for king and if so replace texture
                        if (Board.this.cell_entries[move.finalRow][move.finalCol].equals(CellEntry.whiteKing) && move.finalRow == rows - 1) {

                            counter.setKing(true);

                        } else if (Board.this.cell_entries[move.finalRow][move.finalCol].equals(CellEntry.blackKing) && move.finalRow == 0) {
                            counter.setKing(true);
                        }

                        if (forced) {

                            // check if there is another move in the list
                            if (moves.indexOf(move) < moves.size() - 1) {
                                Gdx.app.log("Dami", "was it forced: " + forced);
                                // tell game screen you are NOT done
                                gameScreen.updateAllTouchable(false);
                                // play the next move
                                gameScreen.applyMovesFromOnline(moves.get(moves.indexOf(move) + 1), moves);
                            }
                            Assets.capture_piece_sound.play();
                        } else {
                            // tell game screen you are done
//                            gameScreen.setPlaying(false);
                            Assets.move_piece_sound.play();
                        }
                    }
                })));
                if (GameScreen.selectIcon != null && GameScreen.selectIcon.getStage() != null) {
                    GameScreen.selectIcon.remove();
                }
            }
        }

        this.cell_entries[move.finalRow][move.finalCol] = this.cell_entries[move.initialRow][move.initialCol];
        this.cell_entries[move.initialRow][move.initialCol] = CellEntry.empty;

        // Promote To King
        if (this.cell_entries[move.finalRow][move.finalCol].equals(CellEntry.white) && move.finalRow == rows - 1) {
            this.cell_entries[move.finalRow][move.finalCol] = CellEntry.whiteKing;

        } else if (this.cell_entries[move.finalRow][move.finalCol].equals(CellEntry.black) && move.finalRow == 0) {
            this.cell_entries[move.finalRow][move.finalCol] = CellEntry.blackKing;
        }
    }

    private boolean canReleaseTurn(int r, int c, Piece counter, Move last_move) {
        if (GameScreen.turn.equals(Player.white)) {
            Vector<Move> moves = White.ObtainForcedMovesForWhite(r, c, this);
            if (!counter.isKing()) {
                return moves.isEmpty();
            } else {
//                for (Move move : moves) {
//                    Gdx.app.log("DamiReleaseTurnFirstMoves", move.initialRow + "," + move.initialCol + " : " + move.finalRow + "," + move.finalCol);
//                }
                Vector<Move> filtered_moves = filterIllegalNextKingMoves(moves, last_move);
//                for (Move move : filtered_moves) {
//                    Gdx.app.log("DamiReleaseTurn", move.initialRow + "," + move.initialCol + " : " + move.finalRow + "," + move.finalCol);
//                }
                return filtered_moves.isEmpty();
            }
        } else {
            Vector<Move> moves = Black.ObtainForcedMovesForBlack(r, c, this);
            if (!counter.isKing()) {
                return moves.isEmpty();
            } else {
//                for (Move move : moves) {
//                    Gdx.app.log("DamiReleaseTurnFirstMoves", move.initialRow + "," + move.initialCol + " : " + move.finalRow + "," + move.finalCol);
//                }
                Vector<Move> filtered_moves = filterIllegalNextKingMoves(moves, last_move);
//                for (Move move : filtered_moves) {
//                    Gdx.app.log("DamiReleaseTurn", move.initialRow + "," + move.initialCol + " : " + move.finalRow + "," + move.finalCol);
//                }
                return filtered_moves.isEmpty();
            }
        }
    }

    private Vector<Move> filterIllegalNextKingMoves(Vector<Move> moves, Move last_move) {

        MoveDir last_move_dir = getMoveDir(last_move);

//        this is what will be shared
        Vector<Move> moves_copy = new Vector<Move>();

        for (Move move : moves) {
//            add to empty copy if it does not fail the test
            if(!isMoveInSameDirection(getMoveDir(move), last_move_dir)) {
                moves_copy.add(move);
            }
        }

        return moves_copy;
    }

    private boolean isMoveInSameDirection(MoveDir current_move_dir, MoveDir last_move_dir) {

        Gdx.app.log("DamiFiltration", "current: " + current_move_dir + " last: " + last_move_dir);

        if(current_move_dir.equals(last_move_dir)) {
            Gdx.app.log("DamiFiltration", "same direction");
            return true;
        }

        switch (last_move_dir) {
            case forwardLeft:
                if(current_move_dir.equals(MoveDir.backwardRight)) {
                    Gdx.app.log("DamiFiltration", "reverse forward left");
                    return true;
                }
                break;
            case backwardRight:
                if(current_move_dir.equals(MoveDir.forwardLeft)) {
                    Gdx.app.log("DamiFiltration", "reverse backward right");
                    return true;
                }
                break;
            case forwardRight:
                if(current_move_dir.equals(MoveDir.backwardLeft)) {
                    Gdx.app.log("DamiFiltration", "reverse forward right");
                    return true;
                }
                break;
            case backwardLeft:
                if(current_move_dir.equals(MoveDir.forwardRight)) {
                    Gdx.app.log("DamiFiltration", "reverse backward left");
                    return true;
                }
                break;
        }

//        all the above cases failed so return false
        return false;
    }

    private void removePiece(int row, int col) {
        Gdx.app.log("Dami", "removePiece called");
        if (gameScreen != null) {
            Gdx.app.log("Dami", "removePiece called + gamescreen is not null");
            Vector2 position = getPieceCoordinates(row, col);

            final Piece piece = (Piece) gameScreen.stage.hit(position.x, position.y, false);
            piece.setTouchable(Touchable.disabled);
            piece.addAction(Actions.sequence(Actions.fadeOut(0.3f), Actions.run(new Runnable() {
                @Override
                public void run() {
                    piece.remove();
                }
            })));
        }
    }

    public Vector2 getPieceCoordinates(int row, int col) {
        float x = (col * 50) + 21 + 4;
        float y = (row * 50) + 22 + 4 + 205;

        return new Vector2(x, y);
    }

    public Vector2 getBoxCoordinates(int row, int col) {
        float x = (col * 50) + 21;
        float y = (row * 50) + 22 + 205;

        return new Vector2(x, y);
    }

    public int[] getPieceRowAndCol(Vector2 coord) {
        int row = (int) ((coord.x / 50) - 21 - 4);
        int col = (int) ((coord.y / 50) - 21 - 4 - 205);

        return new int[]{row, col};
    }

    public int[] getBoxRowAndCol(Vector2 coord) {
        int row = (int) ((coord.x / 50) - 21);
        int col = (int) ((coord.y / 50) - 21 - 205);

        return new int[]{row, col};
    }

    // Capture Black Piece and Move
    public void CaptureBlackPiece(int r1, int c1, int r2, int c2) {
        // Check Valid Capture
        assert (Math.abs(r2 - r1) == 2 && Math.abs(c2 - c1) == 2);

        // Obtain the capture direction
        MoveDir dir = r2 > r1 ? (c2 > c1 ? MoveDir.forwardRight : MoveDir.forwardLeft)
                : (c2 > c1 ? MoveDir.backwardRight : MoveDir.backwardLeft);

        // Removing Black Piece from the board
        switch (dir) {
            case forwardLeft:
                this.cell_entries[r1 + 1][c1 - 1] = CellEntry.empty;
                removePiece(r1 + 1, c1 - 1);
                break;
            case forwardRight:
                this.cell_entries[r1 + 1][c1 + 1] = CellEntry.empty;
                removePiece(r1 + 1, c1 + 1);
                break;
            case backwardLeft:
                this.cell_entries[r1 - 1][c1 - 1] = CellEntry.empty;
                removePiece(r1 - 1, c1 - 1);
                break;
            case backwardRight:
                this.cell_entries[r1 - 1][c1 + 1] = CellEntry.empty;
                removePiece(r1 - 1, c1 + 1);
                break;
        }

        // Decreasing the count of black pieces
        this.blackPieces--;

        // Making move
        this.MakeMove(r1, c1, r2, c2, true);
        //return dir;
    }

    // Capture White Piece and Move
    public void CaptureWhitePiece(int r1, int c1, int r2, int c2) {
        // Check Valid Capture
        assert (Math.abs(r2 - r1) == 2 && Math.abs(c2 - c1) == 2);

        // Obtain the capture direction
        MoveDir dir = r2 < r1 ? (c2 < c1 ? MoveDir.forwardRight : MoveDir.forwardLeft)
                : (c2 < c1 ? MoveDir.backwardRight : MoveDir.backwardLeft);

        // Removing White Piece from the board
        switch (dir) {
            case forwardLeft:
                this.cell_entries[r1 - 1][c1 + 1] = CellEntry.empty;
                removePiece(r1 - 1, c1 + 1);
                break;
            case forwardRight:
                this.cell_entries[r1 - 1][c1 - 1] = CellEntry.empty;
                removePiece(r1 - 1, c1 - 1);
                break;
            case backwardLeft:
                this.cell_entries[r1 + 1][c1 + 1] = CellEntry.empty;
                removePiece(r1 + 1, c1 + 1);
                break;
            case backwardRight:
                this.cell_entries[r1 + 1][c1 - 1] = CellEntry.empty;
                removePiece(r1 + 1, c1 - 1);
                break;
        }

        // Decreasing the count of white pieces
        this.whitePieces--;

        // Making move
        this.MakeMove(r1, c1, r2, c2, true);
        //return dir;
    }

    /**
     * Makes all kinds of valid moves of a white player.
     *
     * @param move
     */
    public void genericMakeWhiteMove(Move move) {
        int r1 = move.initialRow;
        int c1 = move.initialCol;
        int r2 = move.finalRow;
        int c2 = move.finalCol;

        boolean forced = captureBlackPiecesIfAny(r1, c1, r2, c2);

        MakeMove(r1, c1, r2, c2, forced);
    }

    /**
     * Makes all kinds of valid moves of a black player.
     */
    public void genericMakeBlackMove(Move move) {
        int r1 = move.initialRow;
        int c1 = move.initialCol;
        int r2 = move.finalRow;
        int c2 = move.finalCol;

        boolean forced = captureWhitePiecesIfAny(r1, c1, r2, c2);

        MakeMove(r1, c1, r2, c2, forced);

    }

    /**
     * Makes all kinds of valid moves of a white player.
     *
     * @param move
     */
    public void genericMakeWhiteMoveAnimate(Move move, ArrayList<Move> moves) {
        int r1 = move.initialRow;
        int c1 = move.initialCol;
        int r2 = move.finalRow;
        int c2 = move.finalCol;

        boolean forced = captureBlackPiecesIfAny(r1, c1, r2, c2);

        MakeMoveAnimation(move, forced, moves);
    }

    /**
     * Makes all kinds of valid moves of a black player.
     */
    public void genericMakeBlackMoveAnimate(Move move, ArrayList<Move> moves) {
        int r1 = move.initialRow;
        int c1 = move.initialCol;
        int r2 = move.finalRow;
        int c2 = move.finalCol;

        boolean forced = captureWhitePiecesIfAny(r1, c1, r2, c2);

        MakeMoveAnimation(move, forced, moves);

    }

    public MoveDir getMoveDir(Move move) {

        int delta_r = move.finalRow - move.initialRow;
        int delta_c = move.finalCol - move.initialCol;

        if (delta_r > 0 && delta_c > 0) {
            return MoveDir.forwardRight;
        }
        if (delta_r > 0 && delta_c < 0) {
            return MoveDir.forwardLeft;
        }
        if (delta_r < 0 && delta_c < 0) {
            return MoveDir.backwardLeft;
        }
        if (delta_r < 0 && delta_c > 0) {
            return MoveDir.backwardRight;
        }

        return null;
    }

    public static boolean onSameDiagonal(int r1, int c1, int r2, int c2) {
        // same diagonal, implies length between their x and y axes are equal
        return Math.abs(r2 - r1) == Math.abs(c2 - c1);
    }


    public static ArrayList<int[]> getBoxesInDiagonal(int r, int c, MoveDir moveDir) {
        ArrayList<int[]> boxes = new ArrayList<int[]>();
        int i = r;
        int j = c;
        switch (moveDir) {
            case forwardLeft:
                while (j > 0 && i < rows - 1) {
                    i++;
                    j--;
                    boxes.add(new int[]{i, j});
                }
                break;
            case forwardRight:
                while (j < cols - 1 && i < rows - 1) {
                    i++;
                    j++;
                    boxes.add(new int[]{i, j});
                }
                break;
            case backwardLeft:
                while (j > 0 && i > 0) {
                    i--;
                    j--;
                    boxes.add(new int[]{i, j});
                }
                break;
            case backwardRight:
                while (j < cols - 1 && i > 0) {
                    i--;
                    j++;
                    boxes.add(new int[]{i, j});
                }
                break;
        }


        return boxes;
    }

    public static ArrayList<int[]> getBoxesInDiagonal(int r1, int c1, int r2, int c2, MoveDir moveDir) {
        ArrayList<int[]> boxes = new ArrayList<int[]>();
        int i = r1;
        int j = c1;
        switch (moveDir) {
            case forwardLeft:
                while (j > c2 && i < r2) {
                    i++;
                    j--;
                    boxes.add(new int[]{i, j});
                }
                break;
            case forwardRight:
                while (j < c2 && i < r2) {
                    i++;
                    j++;
                    boxes.add(new int[]{i, j});
                }
                break;
            case backwardLeft:
                while (j > c2 && i > r2) {
                    i--;
                    j--;
                    boxes.add(new int[]{i, j});
                }
                break;
            case backwardRight:
                while (j < c2 && i > r2) {
                    i--;
                    j++;
                    boxes.add(new int[]{i, j});
                }
                break;
        }


        return boxes;
    }

    private boolean captureBlackPiecesIfAny(int r1, int c1, int r2, int c2) {

        boolean outcome = false;
        MoveDir moveDir = getMoveDir(new Move(r1, c1, r2, c2));

        for (int[] box : getBoxesInDiagonal(r1, c1, r2, c2, moveDir)) {
            if (this.cell_entries[box[0]][box[1]] == CellEntry.black || this.cell_entries[box[0]][box[1]] == CellEntry.blackKing) {
                capture(box[0], box[1], Player.black);
                outcome = true;
            }
        }
        return outcome;
    }

    private boolean captureWhitePiecesIfAny(int r1, int c1, int r2, int c2) {

        boolean outcome = false;
        MoveDir moveDir = getMoveDir(new Move(r1, c1, r2, c2));

        for (int[] box : getBoxesInDiagonal(r1, c1, r2, c2, moveDir)) {
            if (this.cell_entries[box[0]][box[1]] == CellEntry.white || this.cell_entries[box[0]][box[1]] == CellEntry.whiteKing) {
                capture(box[0], box[1], Player.white);
                outcome = true;
            }
        }
        return outcome;
    }

    private void capture(int r, int c, Player color) {
        removePiece(r, c);
        this.cell_entries[r][c] = CellEntry.empty;
        switch (color) {
            case black:
                blackPieces--;
                break;
            case white:
                whitePieces--;
                break;
        }
    }

    public boolean whiteKingCanMakeMove(int r1, int c1, int r2, int c2) {

//        no need to evaluate anything else
        if (!onSameDiagonal(r1, c1, r2, c2)) {
            Gdx.app.log("DamiMoves", "returned false");
            return false;
        }

        MoveDir moveDir = getMoveDir(new Move(r1, c1, r2, c2));
        ArrayList<int[]> boxes = getBoxesInDiagonal(r1, c1, r2, c2, moveDir);
        int[] next_box = null;

        for (int[] box : boxes) {
            try {
                next_box = boxes.get(boxes.indexOf(box) + 1);
            } catch (IndexOutOfBoundsException e) {
                next_box = null;
            }
//            cannot jump over someone of your same color
            if (this.cell_entries[box[0]][box[1]] == CellEntry.white || this.cell_entries[box[0]][box[1]] == CellEntry.whiteKing) {
                return false;
            }
//            if there are two of opposing PIECE you cannot jump over
            if ((this.cell_entries[box[0]][box[1]] == CellEntry.black || this.cell_entries[box[0]][box[1]] == CellEntry.blackKing)) {
                if (next_box != null) {
                    if ((this.cell_entries[next_box[0]][next_box[1]] == CellEntry.black || this.cell_entries[next_box[0]][next_box[1]] == CellEntry.blackKing))
                        return false;
                }
            }
        }

        return true;
    }

    public boolean blackKingCanMakeMove(int r1, int c1, int r2, int c2) {

//        no need to evaluate anything else
        if (!onSameDiagonal(r1, c1, r2, c2)) {
            Gdx.app.log("DamiMoves", "returned false");
            return false;
        }

        MoveDir moveDir = getMoveDir(new Move(r1, c1, r2, c2));
        ArrayList<int[]> boxes = getBoxesInDiagonal(r1, c1, r2, c2, moveDir);
        int[] next_box = null;

        for (int[] box : boxes) {
            try {
                next_box = boxes.get(boxes.indexOf(box) + 1);
            } catch (IndexOutOfBoundsException e) {
                next_box = null;
            }
//            cannot jump over someone of your same color
            if (this.cell_entries[box[0]][box[1]] == CellEntry.black || this.cell_entries[box[0]][box[1]] == CellEntry.blackKing) {
                return false;
            }
//            if there are two of opposing PIECE you cannot jump over
            if ((this.cell_entries[box[0]][box[1]] == CellEntry.white || this.cell_entries[box[0]][box[1]] == CellEntry.whiteKing)) {
                if (next_box != null) {
                    if ((this.cell_entries[next_box[0]][next_box[1]] == CellEntry.white || this.cell_entries[next_box[0]][next_box[1]] == CellEntry.whiteKing))
                        return false;
                }
            }
        }

        return true;
    }

    public static int[] GetNextBox(ArrayList<int[]> boxes, int[] box) {
        int[] next_box;
        try {
            next_box = boxes.get(boxes.indexOf(box) + 1);
        } catch (IndexOutOfBoundsException e) {
            next_box = null;
        }

        return next_box;
    }

    public void Display() {
        this.DisplayColIndex();
        this.DrawHorizontalLine();

        for (int i = rows - 1; i >= 0; i--) {
            this.DisplayRowIndex(i);
            this.DrawVerticalLine();

            for (int j = 0; j < cols; j++) {
                System.out.print(this.BoardPiece(i, j));
                this.DrawVerticalLine();
            }

            this.DisplayRowIndex(i);
            System.out.println();
            this.DrawHorizontalLine();
        }

        this.DisplayColIndex();
        System.out.println();
    }

    private String BoardPiece(int i, int j) {
        assert (i > 0 && i < rows && j > 0 && j < cols);
        String str = new String();

        if (this.cell_entries[i][j] == CellEntry.inValid) {
            str = "     ";
        } else if (this.cell_entries[i][j] == CellEntry.empty) {
            str = "  _  ";
        } else if (this.cell_entries[i][j] == CellEntry.white) {
            str = "  W  ";
        } else if (this.cell_entries[i][j] == CellEntry.black) {
            str = "  B  ";
        } else if (this.cell_entries[i][j] == CellEntry.whiteKing) {
            str = "  W+ ";
        } else if (this.cell_entries[i][j] == CellEntry.blackKing) {
            str = "  B+ ";
        }

        return str;
    }

    private void DrawHorizontalLine() {
        System.out.println("    _______________________________________________");
    }

    private void DrawVerticalLine() {
        System.out.print("|");
    }

    private void DisplayColIndex() {
        System.out.print("   ");
        for (int colIndex = 0; colIndex < cols; colIndex++) {
            System.out.print("   " + colIndex + "  ");
        }
        System.out.println();
    }

    private void DisplayRowIndex(int i) {
        System.out.print(" " + i + " ");
    }


    public Board duplicate() {
        Board newBoard = new Board(this.cell_entries);
        newBoard.blackPieces = this.blackPieces;
        newBoard.whitePieces = this.whitePieces;

        return newBoard;
    }


    public boolean CheckGameComplete() {
        Gdx.app.log("Dami", "black pieces " + this.blackPieces + " white pieces " + this.whitePieces);
        return (this.blackPieces == 0 || this.whitePieces == 0) ? true : false;
    }

    //    get the next player
    public static Player getOtherTurn(Player current_turn) {
        if (current_turn.equals(Player.white)) {
            return Player.black;
        } else {
            return Player.white;
        }
    }


    public boolean CheckGameDraw(Player turn) {

        Vector<Vector<Move>> possibleMoveSeq = Robot.expandMoves(this.duplicate(), turn);
//        Gdx.app.log("randomLog", possibleMoveSeq.get(0).get(0).initialRow + ", " + possibleMoveSeq.get(0).get(0).initialCol + ", " + possibleMoveSeq.get(0).get(0).finalRow + ", " + possibleMoveSeq.get(0).get(0).finalCol + ", ");

        if (possibleMoveSeq.isEmpty()) {
            return true;

        } else {
            return false;
        }
    }

    public boolean isWhiteWinner() {
        boolean res = false;
        if (this.blackPieces == 0) {
            res = true;
        }
        return res;
    }

    public boolean isBlackWinner() {
        boolean res = false;
        if (this.whitePieces == 0) {
            res = true;
        }
        return res;
    }
}