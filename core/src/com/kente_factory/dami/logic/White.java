/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kente_factory.dami.logic;

import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.enumerations.MoveDir;
import com.kente_factory.dami.enumerations.Owner;

import java.util.ArrayList;
import java.util.Vector;

/**
 * @author WAHIB
 */
public class White {
    static Owner owner;

    public static void Move() {
        UserInteractions.PrintSeparator('-');
        System.out.println("\t\tWHITE's TURN");
        UserInteractions.PrintSeparator('-');

        if (owner.equals(Owner.HUMAN)) {

            Human.makeNextWhiteMoves();

        } else {

            assert (owner.equals(Owner.ROBOT));
            Robot.makeNextWhiteMoves();

        }
    }


    public static Vector<Move> ObtainForcedMovesForWhite(int r, int c, Board board) {
        Vector<Move> furtherCaptures = new Vector<Move>();

        if (board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.white) || board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
            if (ForwardLeftCaptureForWhite(r, c, board) != null)
                furtherCaptures.add(ForwardLeftCaptureForWhite(r, c, board));
            if (ForwardRightCaptureForWhite(r, c, board) != null)
                furtherCaptures.add(ForwardRightCaptureForWhite(r, c, board));

            if (BackwardLeftCaptureForWhite(r, c, board) != null)
                furtherCaptures.add(BackwardLeftCaptureForWhite(r, c, board));
            if (BackwardRightCaptureForWhite(r, c, board) != null)
                furtherCaptures.add(BackwardRightCaptureForWhite(r, c, board));
        }

//        white king captures in all directions
        if (board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
            if (GetCapturesForWhiteKing(r, c, board) != null) {
                furtherCaptures.addAll(GetCapturesForWhiteKing(r, c, board));
            }
        }

        return furtherCaptures;
    }

    public static Vector<Move> CalculateAllForcedMovesForWhite(Board board) {
        Vector<Move> forcedMovesForWhite = new Vector<Move>();

        // Scan across the board
        for (int r = 0; r < Board.rows; r++) {
            // Check only valid cols
            int c = (r % 2 == 0) ? 0 : 1;
            for (; c < Board.cols; c += 2) {
                assert (!board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.inValid));

                // Forward Capture
                if (
                        board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.white) ||
                                board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.whiteKing)
                        ) {
                    // Boundary Condition for forward capture
                    if (r < Board.rows - 2) {
                        // Forward Left Capture
                        if (ForwardLeftCaptureForWhite(r, c, board) != null)
                            forcedMovesForWhite.add(ForwardLeftCaptureForWhite(r, c, board));

                        // Forward Right Capture
                        if (ForwardRightCaptureForWhite(r, c, board) != null)
                            forcedMovesForWhite.add(ForwardRightCaptureForWhite(r, c, board));
                    }

                    // Boundary Condition for backward capture
                    if (r >= 2) {
                        // Backward Left Capture
                        if (BackwardLeftCaptureForWhite(r, c, board) != null)
                            forcedMovesForWhite.add(BackwardLeftCaptureForWhite(r, c, board));

                        // Backward Right Capture
                        if (BackwardRightCaptureForWhite(r, c, board) != null)
                            forcedMovesForWhite.add(BackwardRightCaptureForWhite(r, c, board));
                    }
                }
                // whiteking captures in all directions
                if (board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                    if (GetCapturesForWhiteKing(r, c, board) != null) {
                        forcedMovesForWhite.addAll(GetCapturesForWhiteKing(r, c, board));
                    }
                }
            }
        }

        return forcedMovesForWhite;
    }

    private static Vector<Move> GetCapturesForWhiteKing(int r, int c, Board board) {

        Vector<Move> forced_moves = new Vector<Move>();

        ArrayList<int[]> boxes_fl = Board.getBoxesInDiagonal(r, c, com.kente_factory.dami.enumerations.MoveDir.forwardLeft);
        ArrayList<int[]> boxes_fr = Board.getBoxesInDiagonal(r, c, com.kente_factory.dami.enumerations.MoveDir.forwardRight);
        ArrayList<int[]> boxes_bl = Board.getBoxesInDiagonal(r, c, com.kente_factory.dami.enumerations.MoveDir.backwardLeft);
        ArrayList<int[]> boxes_br = Board.getBoxesInDiagonal(r, c, com.kente_factory.dami.enumerations.MoveDir.backwardRight);

//        forward left
        for (int[] box : boxes_fl) {
//            if there is a white PIECE along this line, stop adding
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                break;
            }

//            if there is a black PIECE...
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {

                ArrayList<int[]> next_boxes = Board.getBoxesInDiagonal(box[0], box[1], com.kente_factory.dami.enumerations.MoveDir.forwardLeft);

                if (!next_boxes.isEmpty()) {
                    int[] first_next_box = next_boxes.get(0);

//                    if the direct following box is a black PIECE, break and stop searching in all
                    if (board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black ||
                            board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                        break;
                    }
                }

                for (int[] next_box : next_boxes) {

//              if you meet a white PIECE along the line, stop searching
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                        break;
                    }

//              if you meet a black PIECE along the line, go back to the loop
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {
                        break;
                    }

//            ...and followed by an empty space
                    if (board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.empty) {
                        forced_moves.add(new Move(r, c, next_box[0], next_box[1]));
                    }
                }
            }
        }

//        forward right
        for (int[] box : boxes_fr) {
//            if there is a white PIECE along this line, stop adding
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                break;
            }

//            if there is a black PIECE...
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {

                ArrayList<int[]> next_boxes = Board.getBoxesInDiagonal(box[0], box[1], com.kente_factory.dami.enumerations.MoveDir.forwardRight);

                if (!next_boxes.isEmpty()) {
                    int[] first_next_box = next_boxes.get(0);

//                    if the direct following box is a black PIECE, break and stop searching in all
                    if (board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black ||
                            board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                        break;
                    }
                }

                for (int[] next_box : next_boxes) {

//              if you meet a white PIECE along the line, stop searching
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                        break;
                    }

//              if you meet a black PIECE along the line, go back to the loop
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {
                        break;
                    }

//            ...and followed by an empty space
                    if (board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.empty) {
                        forced_moves.add(new Move(r, c, next_box[0], next_box[1]));
                    }
                }
            }
        }

//        backward left
        for (int[] box : boxes_bl) {
//            if there is a white PIECE along this line, stop adding
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                break;
            }

//            if there is a black PIECE...
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {

                ArrayList<int[]> next_boxes = Board.getBoxesInDiagonal(box[0], box[1], com.kente_factory.dami.enumerations.MoveDir.backwardLeft);

                if (!next_boxes.isEmpty()) {
                    int[] first_next_box = next_boxes.get(0);

//                    if the direct following box is a black PIECE, break and stop searching in all
                    if (board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black ||
                            board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                        break;
                    }
                }

                for (int[] next_box : next_boxes) {

//              if you meet a white PIECE along the line, stop searching
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                        break;
                    }

//              if you meet a black PIECE along the line, go back to the loop
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {
                        break;
                    }

//            ...and followed by an empty space
                    if (board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.empty) {
                        forced_moves.add(new Move(r, c, next_box[0], next_box[1]));
                    }
                }
            }
        }

//        backward right
        for (int[] box : boxes_br) {
//            if there is a white PIECE along this line, stop adding
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                break;
            }

//            if there is a black PIECE...
            if ((board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[box[0]][box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {

                ArrayList<int[]> next_boxes = Board.getBoxesInDiagonal(box[0], box[1], MoveDir.backwardRight);

                if (!next_boxes.isEmpty()) {
                    int[] first_next_box = next_boxes.get(0);

//                    if the direct following box is a black PIECE, break and stop searching in all
                    if (board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black ||
                            board.cell_entries[first_next_box[0]][first_next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                        break;
                    }
                }

                for (int[] next_box : next_boxes) {

//              if you meet a white PIECE along the line, stop searching
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)) {
                        break;
                    }

//              if you meet a black PIECE along the line, go back to the loop
                    if ((board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.blackKing)) {
                        break;
                    }

//            ...and followed by an empty space
                    if (board.cell_entries[next_box[0]][next_box[1]] == com.kente_factory.dami.enumerations.CellEntry.empty) {
                        forced_moves.add(new Move(r, c, next_box[0], next_box[1]));
                    }
                }
            }
        }

        return forced_moves;
    }


    /**
     * Returns a vector of all possible moves which White can make at the state of the game given by board.
     * <p>
     * Should only be called if no forced moves exist.
     *
     * @param board
     * @return
     */
    public static Vector<Move> CalculateAllNonForcedMovesForWhite(Board board) {

        Vector<Move> allNonForcedMovesForWhite = new Vector<Move>();

        // Scan across the board
        for (int r = 0; r < Board.rows; r++) {
            // Check only valid cols
            int c = (r % 2 == 0) ? 0 : 1;
            for (; c < Board.cols; c += 2) {
                assert (!board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.inValid));

                // Forward Move for normal white PIECE.
                if (board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.white)) {

                    Move move = null;
                    move = ForwardLeftCaptureForWhite(r, c, board);
                    assert (move == null);
                    move = ForwardRightCaptureForWhite(r, c, board);
                    assert (move == null);

                    move = ForwardLeftForWhite(r, c, board);
                    if (move != null) {
                        allNonForcedMovesForWhite.add(move);
                    }

                    move = ForwardRightForWhite(r, c, board);
                    if (move != null) {
                        allNonForcedMovesForWhite.add(move);
                    }
                }

                //Forward and Backward Move for black king PIECE.
                if (board.cell_entries[r][c] == com.kente_factory.dami.enumerations.CellEntry.whiteKing) {
                    Move move = null;
                    move = ForwardLeftCaptureForWhite(r, c, board);
                    assert (move == null);
                    move = ForwardRightCaptureForWhite(r, c, board);
                    assert (move == null);

                    move = BackwardLeftCaptureForWhite(r, c, board);
                    assert (move == null);
                    move = BackwardRightCaptureForWhite(r, c, board);
                    assert (move == null);

                    move = ForwardLeftForWhite(r, c, board);
                    if (move != null) {
                        allNonForcedMovesForWhite.add(move);
                    }

                    move = ForwardRightForWhite(r, c, board);
                    if (move != null) {
                        allNonForcedMovesForWhite.add(move);
                    }

                    move = BackwardLeftForWhite(r, c, board);
                    if (move != null) {
                        allNonForcedMovesForWhite.add(move);
                    }

                    move = BackwardRightForWhite(r, c, board);
                    if (move != null) {
                        allNonForcedMovesForWhite.add(move);
                    }

                }


            }
        }

        return allNonForcedMovesForWhite;
    }


    private static Move ForwardLeftForWhite(int r, int c, Board board) {
        Move forwardLeft = null;
        if (r < Board.rows - 1 && c >= 1 &&
                board.cell_entries[r + 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.empty
                ) {
            forwardLeft = new Move(r, c, r + 1, c - 1);
        }
        return forwardLeft;
    }

    // Forward Left Capture for White
    private static Move ForwardLeftCaptureForWhite(int r, int c, Board board) {
        Move forwardLeftCapture = null;

        if (r < Board.rows - 2 && c >= 2 &&
                (
                        board.cell_entries[r + 1][c - 1].equals(com.kente_factory.dami.enumerations.CellEntry.black)
                                || board.cell_entries[r + 1][c - 1].equals(com.kente_factory.dami.enumerations.CellEntry.blackKing)
                )
                && board.cell_entries[r + 2][c - 2].equals(com.kente_factory.dami.enumerations.CellEntry.empty)
                ) {
            forwardLeftCapture = new Move(r, c, r + 2, c - 2);
            //System.out.println("Forward Left Capture");
        }

        return forwardLeftCapture;
    }

    private static Move ForwardRightForWhite(int r, int c, Board board) {
        Move forwardRight = null;
        if (r < Board.rows - 1 && c < Board.cols - 1 &&
                board.cell_entries[r + 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.empty
                ) {
            forwardRight = new Move(r, c, r + 1, c + 1);
        }
        return forwardRight;
    }

    // Forward Right Capture for White
    private static Move ForwardRightCaptureForWhite(int r, int c, Board board) {
        Move forwardRightCapture = null;

        if (r < Board.rows - 2 && c < Board.cols - 2 &&
                (
                        board.cell_entries[r + 1][c + 1].equals(com.kente_factory.dami.enumerations.CellEntry.black)
                                || board.cell_entries[r + 1][c + 1].equals(com.kente_factory.dami.enumerations.CellEntry.blackKing)
                )
                && board.cell_entries[r + 2][c + 2].equals(com.kente_factory.dami.enumerations.CellEntry.empty)
                ) {
            forwardRightCapture = new Move(r, c, r + 2, c + 2);
            //System.out.println("Forward Right Capture");
        }

        return forwardRightCapture;
    }

    private static Move BackwardLeftForWhite(int r, int c, Board board) {
        Move backwardLeft = null;
        if (r >= 1 && c >= 1 &&
                board.cell_entries[r - 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.empty
                ) {
            backwardLeft = new Move(r, c, r - 1, c - 1);
        }
        return backwardLeft;
    }

    // Backward Left Capture for White
    private static Move BackwardLeftCaptureForWhite(int r, int c, Board board) {

        Move backwardLeftCapture = null;

        if (r >= 2 && c >= 2 && (
                board.cell_entries[r - 1][c - 1].equals(com.kente_factory.dami.enumerations.CellEntry.black)
                        || board.cell_entries[r - 1][c - 1].equals(com.kente_factory.dami.enumerations.CellEntry.blackKing)
        )
                && board.cell_entries[r - 2][c - 2].equals(com.kente_factory.dami.enumerations.CellEntry.empty)
                ) {
            backwardLeftCapture = new Move(r, c, r - 2, c - 2);
            //System.out.println("Backward Left Capture");
        }

        return backwardLeftCapture;
    }

    private static Move BackwardRightForWhite(int r, int c, Board board) {
        Move backwardRight = null;
        if (r >= 1 && c < Board.cols - 1 &&
                board.cell_entries[r - 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.empty
                ) {
            backwardRight = new Move(r, c, r - 1, c + 1);
        }
        return backwardRight;
    }

    // Backward Right Capture for White
    private static Move BackwardRightCaptureForWhite(int r, int c, Board board) {
        Move backwardRightCapture = null;

        if (r >= 2 && c < Board.cols - 2 && (
                board.cell_entries[r - 1][c + 1].equals(com.kente_factory.dami.enumerations.CellEntry.black) ||
                        board.cell_entries[r - 1][c + 1].equals(com.kente_factory.dami.enumerations.CellEntry.blackKing)
        )
                && board.cell_entries[r - 2][c + 2].equals(CellEntry.empty)
                ) {
            backwardRightCapture = new Move(r, c, r - 2, c + 2);
            //System.out.println("Backward Right Capture");
        }

        return backwardRightCapture;
    }
}