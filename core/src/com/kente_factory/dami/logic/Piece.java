package com.kente_factory.dami.logic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kente_factory.dami.Assets;
import com.kente_factory.dami.Entity;
import com.kente_factory.dami.LocalUserData;
import com.kente_factory.dami.enumerations.MatchType;
import com.kente_factory.dami.enumerations.store_item_types.CrownType;
import com.kente_factory.dami.enumerations.store_item_types.PieceShape;
import com.kente_factory.dami.SelectIcon;
import com.kente_factory.dami.enumerations.Player;

/**
 * Created by wahib on 12/27/2017.
 */

public class Piece extends Entity {

    private static final Texture[] textures = {Assets.circle_white_pieceTexture, Assets.circle_black_pieceTexture};

    public int col, row;
    private com.kente_factory.dami.enumerations.Player player;

    private PieceShape shape;
    private CrownType crownType;

    private TextureRegion crown_TextureRegion;

    private boolean king;

    private float rotate_factor;
    private float crown_padding;

    private GameScreen gameScreen;

    public Piece(GameScreen gameScreen, com.kente_factory.dami.enumerations.Player player, int col, int row, PieceShape pieceShape, CrownType crownType) {
        super(textures[player.ordinal()], col * 50 + 21 + 4, row * 50 + 22 + 4 + 205);

        this.col = col;
        this.row = row;
        this.player = player;

        this.rotate_factor = 1;

        this.gameScreen = gameScreen;

        this.setTouchable(Touchable.enabled);
        this.setOrigin(this.getWidth() / 2, this.getHeight() / 2);

        addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                selectMe();
                super.touchUp(event, x, y, pointer, button);
            }
        });

        if (gameScreen.getMatchType().equals(MatchType.local)) {
            this.shape = LocalUserData.pieceShape;
            this.crownType = LocalUserData.crownType;
        }

        if (gameScreen.getMatchType().equals(MatchType.online)) {
            this.shape = pieceShape;
            this.crownType = crownType;
        }

        setShapeTexture(this.shape, this.player);
        setCrownTextureRegion(this.crownType);
    }

    private void setCrownTextureRegion(CrownType crownType) {
        switch (crownType) {
            case normal:
                this.crown_TextureRegion = Piece.this.getRegion();
                this.crown_padding = 9;
                break;
            case king:
                this.crown_TextureRegion = new TextureRegion(Assets.crown_kingTexture);
                this.crown_padding = 15;
                break;
            case queen:
                this.crown_TextureRegion = new TextureRegion(Assets.crown_queenTexture);
                this.crown_padding = 15;
                break;
            default:
                this.crown_TextureRegion = new TextureRegion(Assets.crown_bowling_hatTexture);
                this.crown_padding = 9;
                break;
        }
    }

    private void setShapeTexture(PieceShape pieceShape, com.kente_factory.dami.enumerations.Player player) {

        switch (pieceShape) {
            case circle:
                if (player.equals(com.kente_factory.dami.enumerations.Player.white)) {
                    this.setTextureRegion(Assets.circle_white_pieceTexture);
                } else {
                    this.setTextureRegion(Assets.circle_black_pieceTexture);
                }
                break;
            case square:
                if (player.equals(com.kente_factory.dami.enumerations.Player.white)) {
                    this.setTextureRegion(Assets.square_white_pieceTexture);
                } else {
                    this.setTextureRegion(Assets.square_black_pieceTexture);
                }
                break;
            case diamond:
                if (player.equals(com.kente_factory.dami.enumerations.Player.white)) {
                    this.setTextureRegion(Assets.diamond_white_pieceTexture);
                } else {
                    this.setTextureRegion(Assets.diamond_black_pieceTexture);
                }
                break;
            case triangle:
                if (player.equals(com.kente_factory.dami.enumerations.Player.white)) {
                    this.setTextureRegion(Assets.triangle_white_pieceTexture);
                } else {
                    this.setTextureRegion(Assets.triangle_black_pieceTexture);
                }
                break;
        }

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
//        super.draw(batch, parentAlpha);

        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

//       *rotate_factor: for rotation if black and black is the owner
        batch.draw(Piece.this.getRegion(), this.getX(), this.getY(), this.getOriginX(),
                this.getOriginY(), this.getWidth(), this.getHeight(),
                this.getScaleX(), this.getScaleY() * rotate_factor, this.getRotation());

        if(isKing()) {
            batch.draw(crown_TextureRegion, this.getX(), this.getY() + (crown_padding * rotate_factor), this.getOriginX(),
                    this.getOriginY(), this.getWidth(), this.getHeight(),
                    this.getScaleX(), this.getScaleY() * rotate_factor, this.getRotation());
        }
        this.setBounds(this.getX(), this.getY(), this.getWidth(),
                this.getHeight());
    }

    public void setRotateFactor(float rotate_factor) {
        this.rotate_factor = rotate_factor;
    }

    public void updateTouchable(boolean all_disabled) {
        if (!gameScreen.isComputerTurn() && !all_disabled) {
            if ((GameScreen.turn.equals(com.kente_factory.dami.enumerations.Player.white) && player.equals(com.kente_factory.dami.enumerations.Player.white)) || (GameScreen.turn.equals(com.kente_factory.dami.enumerations.Player.black) && player.equals(com.kente_factory.dami.enumerations.Player.black))) {
                this.setTouchable(Touchable.enabled);
            } else {
                this.setTouchable(Touchable.disabled);
            }
        } else {
            this.setTouchable(Touchable.disabled);
        }
    }

    public void updateTouchableOnline(boolean all_disabled) {
        if (!all_disabled) {
            if ((GameScreen.turn.equals(com.kente_factory.dami.enumerations.Player.white) && GameScreen.game.getPlatformInterface().isWhiteOwner() && player.equals(com.kente_factory.dami.enumerations.Player.white)) ||
                    (GameScreen.turn.equals(com.kente_factory.dami.enumerations.Player.black) && player.equals(Player.black) && !GameScreen.game.getPlatformInterface().isWhiteOwner())) {
                this.setTouchable(Touchable.enabled);
            } else {
                this.setTouchable(Touchable.disabled);
            }
        } else {
            this.setTouchable(Touchable.disabled);
        }
    }

    public boolean isKing() {
        return king;
    }

    public void setKing(boolean king) {
//        if (this.player.equals(Player.white)) {
//            this.setTextureRegion(Assets.crown_whiteTexture);
//        }
//        if (this.player.equals(Player.black)) {
//            this.setTextureRegion(Assets.crown_blackTexture);
//        }
        this.king = king;
    }

    private void selectMe() {
        // if already selected
        if (GameScreen.selectIcon != null) {
            clearSelect();
        }

        GameScreen.setSelectedPiece(this);

        GameScreen.selectIcon = new SelectIcon(Piece.this, Piece.this.getX() - 4, Piece.this.getY() - 4);
        Piece.this.getStage().addActor(GameScreen.selectIcon);
        GameScreen.selectIcon.setZIndex(Piece.this.getZIndex() - 1);
        gameScreen.setUser_origin(new int[]{Piece.this.row, Piece.this.col});
    }

    public static void clearSelect() {
        GameScreen.setSelectedPiece(null);
        if (GameScreen.selectIcon != null) {
            GameScreen.selectIcon.remove();
        }
    }

    public void playMoveSound() {
        if(LocalUserData.sound_enabled) {
            Assets.move_piece_sound.play();
        }
    }

    public void playCaptureSound() {
        if(LocalUserData.sound_enabled) {
            Assets.capture_piece_sound.play();
        }
    }

    public void setColRow(int col, int row) {
        this.col = col;
        this.row = row;
    }
}
