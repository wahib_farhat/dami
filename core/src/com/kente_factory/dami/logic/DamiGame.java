/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kente_factory.dami.logic;

import com.kente_factory.dami.enumerations.Player;

/**
 *
 * @author WAHIB
 */
public class DamiGame {
    
    static Board board;
    
    public DamiGame()
    {
        this.Initialize();
    }
    
    public void PlayGame()
    {   
        while(!DamiGame.board.CheckGameComplete())
        {  
            if(DamiGame.board.CheckGameDraw(com.kente_factory.dami.enumerations.Player.white)){
                break;
            }
            
            White.Move();
            if(DamiGame.board.CheckGameComplete()){
                UserInteractions.DisplayGreetings(com.kente_factory.dami.enumerations.Player.white);
                DamiGame.board.Display();
                break;
            }

            if(DamiGame.board.CheckGameDraw(com.kente_factory.dami.enumerations.Player.black)){
                break;
            }
            
            /////////////////////////////////////////
            //System.out.println("Black ="+Game.board.blackPieces+", White="+Game.board.whitePieces);
            /////////////////////////////////////////
            DamiGame.board.Display();
            
            Black.Move();
            if(DamiGame.board.CheckGameComplete()){
                UserInteractions.DisplayGreetings(Player.black);
                DamiGame.board.Display();
                break;
            }
            
//            Game.board.Display();           
            /////////////////////////////////////////
//            System.out.println("Black ="+Game.board.blackPieces+", White="+Game.board.whitePieces);
            /////////////////////////////////////////
        }
    }
    
   
    private void Initialize()
    {
        
//        board  = new Board();

//        switch(human)
//        {
//            case 'w':
//                White.owner = Owner.HUMAN;
//                Black.owner = Owner.ROBOT;
//                break;
//            case 'b':
//                White.owner = Owner.ROBOT;
//                Black.owner = Owner.HUMAN;
//                break;
//            case 'a':
//                White.owner = Owner.HUMAN;
//                Black.owner = Owner.HUMAN;
//                break;
//            case 'n':
//                White.owner = Owner.ROBOT;
//                Black.owner = Owner.ROBOT;
//                break;
//        }
    }
}