package com.kente_factory.dami.logic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kente_factory.dami.Assets;
import com.kente_factory.dami.BackButton;
import com.kente_factory.dami.ButtonAction;
import com.kente_factory.dami.Dami;
import com.kente_factory.dami.GameMessage;
import com.kente_factory.dami.QuitGameMessage;
import com.kente_factory.dami.SelectIcon;
import com.kente_factory.dami.Utils;
import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.enumerations.MatchType;
import com.kente_factory.dami.enumerations.Owner;
import com.kente_factory.dami.enumerations.Player;

import java.util.ArrayList;

/**
 * Created by wahib on 12/27/2017.
 */

public class GameScreen implements Screen {

    public Stage stage;
    public Stage HUD;
    public static Dami game;
    public static Board board;

    public static com.kente_factory.dami.enumerations.Player turn;
    private static Piece selectedPiece;
    public static SelectIcon selectIcon;

    private static int[] user_origin;
    private static int[] user_destination;

    private Owner whiteOwner;
    private Owner blackOwner;

    static int changeCalled;

    public enum GAME_OPTIONS {white, black, two_player}

    public GAME_OPTIONS game_options;
    private com.kente_factory.dami.enumerations.MatchType matchType;

    private boolean rotated;

    // check if someone is in the middle of a move or has finished
    private static boolean playing;
    private static boolean game_over;

    public ArrayList<Move> current_moves;

    private String current_match_id;

    public GameScreen(final Dami game, GAME_OPTIONS game_options, final com.kente_factory.dami.enumerations.MatchType matchType, com.kente_factory.dami.enumerations.CellEntry[][] cellEntries, com.kente_factory.dami.enumerations.Player current_turn) {
        GameScreen.game = game;
        stage = new Stage(new StretchViewport(540, 960));
        HUD = new Stage(new StretchViewport(540, 960));

        rotated = false;

        HUD.addActor(new BackButton(new ButtonAction() {
            @Override
            public void onClick() {
                HUD.addActor(new QuitGameMessage(game, matchType, GameScreen.this));
//                make sure all clicks go to you
                Gdx.input.setInputProcessor(HUD);
            }
        }));

        user_origin = null;
        user_destination = null;
        selectedPiece = null;
        changeCalled = 0;
        game_over = false;

        this.matchType = matchType;

        this.current_match_id = null;

        this.game_options = game_options;

        switch (game_options) {
            case black:
                whiteOwner = Owner.ROBOT;
                blackOwner = Owner.HUMAN;
                stage.getCamera().rotate(180, 0, 0, 1);
                break;
            case white:
                whiteOwner = Owner.HUMAN;
                blackOwner = Owner.ROBOT;
                break;
            case two_player:
                whiteOwner = Owner.HUMAN;
                blackOwner = Owner.HUMAN;
                break;
        }

//        setup board positioning and whose turn it is
//        if online match it will be restored after turn data is initialized
        if (matchType.equals(com.kente_factory.dami.enumerations.MatchType.local)) {
            initialize(cellEntries, current_turn, new ArrayList<Move>(), null, new BoardItems());
        }

//        online matches are updated in initialize
        if (matchType.equals(com.kente_factory.dami.enumerations.MatchType.local)) {
            updateAllTouchable(false);
        }

        if (matchType.equals(com.kente_factory.dami.enumerations.MatchType.local)) {
            if (isComputerTurn()) {
                playAturn();
            }
        }

        current_moves = new ArrayList<Move>();
    }

    public void initialize(com.kente_factory.dami.enumerations.CellEntry[][] cellEntries,
                           com.kente_factory.dami.enumerations.Player turn, ArrayList<Move> moves, String match_id, BoardItems boardItems) {
        Gdx.app.log("Dami", "initialize called");

//        reset camera to default position
        stage.getCamera().direction.set(0, 0, -1);
        stage.getCamera().up.set(0, 1, 0);
        stage.getCamera().update();

//        set up existing cells and set turn
        if (cellEntries != null && turn != null) {
            GameScreen.board = new Board(this, cellEntries, boardItems);
            GameScreen.turn = turn;

//            give a black view
            if (!game.getPlatformInterface().isWhiteOwner()) {
                stage.getCamera().rotate(180, 0, 0, 1);
                rotated = true;
                rotateAllPieces();
                board.edge.setRotation(180);
                board.edge.setPosition(0, 747);
            }

        } else
//            create new
        {
            Gdx.app.log("Dami", "new board called");
            GameScreen.board = new Board(this, boardItems);
            GameScreen.turn = com.kente_factory.dami.enumerations.Player.white;

            if (blackOwner.equals(Owner.HUMAN) && whiteOwner.equals(Owner.ROBOT)) {
                stage.getCamera().rotate(180, 0, 0, 1);
                rotated = true;
                rotateAllPieces();
                board.edge.setRotation(180);
                board.edge.setPosition(0, 747);
            }
        }

        updateAllTouchable(false);
        if (!moves.isEmpty()) {
            applyMovesFromOnline(moves.get(0), moves);
        }

//            store how you got the board looks after you have applied the move
        Utils.storeBoardOnlineGameLocal(board.cell_entries);

        this.current_match_id = match_id;

//        before we let the game continue, make sure it is not a draw or the game is not over
//        check draw of the next player's turn
        if(!game_over) {
//            don't even check because locally, we have stored this game as an ended one
            if (board.CheckGameComplete() || board.CheckGameDraw(Board.getOtherTurn(turn))) {
                game.getPlatformInterface().endOnlineMatch();
                locallyProcessEndGame();
            }
        }
    }

    public MatchType getMatchType() {
        return matchType;
    }


    public String getCurrentMatchId() {
        return current_match_id;
    }


    public void applyMovesFromOnline(Move current_move, ArrayList<Move> moves) {
        Gdx.app.log("DAMIT","apply moves is called");
//         apply only the current move and pass all the moves, the next move will be called when the animation is done
        if (turn.equals(com.kente_factory.dami.enumerations.Player.white)) {
            Gdx.app.log("DAMIT","same from white turn");
            GameScreen.board.genericMakeBlackMoveAnimate(current_move, moves);
//                setPlaying(true);
        } else if (turn.equals(com.kente_factory.dami.enumerations.Player.black)) {
            Gdx.app.log("DAMIT","same from black turn");
            GameScreen.board.genericMakeWhiteMoveAnimate(current_move, moves);
//                setPlaying(true);
        }
        if (moves.indexOf(current_move) == moves.size() - 1) {
//            store how you got the board looks after you have applied the last move
            Utils.storeBoardOnlineGameLocal(board.cell_entries);
        }
        Gdx.app.log("DAMIT","outside the block");

    }

    public static void restoreFromTurnData(CellEntry[][] cellEntries) {
        board.setCell_entries(cellEntries);
    }

    public void setUser_origin(int[] origin) {
        Gdx.app.log("randomLog", "setUser_origin called");
        user_origin = origin;
    }

    public void setUser_destination(int[] destination) {
        Gdx.app.log("randomLog", "setUser_destination called");
        user_destination = destination;
        if (user_origin != null) {
            playAturn();
        }
    }

    public static void setSelectedPiece(Piece piece) {
        selectedPiece = piece;
    }

    public static Piece getSelectedPiece() {
        return selectedPiece;
    }

    public void changeTurn() {
        if (matchType.equals(com.kente_factory.dami.enumerations.MatchType.local)) {
//        before we change turn, make sure it is not a draw or the game is not over
//        check draw of the next player's turn
            if (board.CheckGameComplete() || board.CheckGameDraw(Board.getOtherTurn(turn))) {
                game_over = true;

//                pick which message depending on draw or someone won
                if (board.CheckGameComplete()) {
                    HUD.addActor(new com.kente_factory.dami.GameMessage(com.kente_factory.dami.enumerations.MatchType.local, game_options, turn, game));
                } else {
                    HUD.addActor(new com.kente_factory.dami.GameMessage(com.kente_factory.dami.enumerations.MatchType.local, game_options, null, game));
                }
                Gdx.input.setInputProcessor(HUD);
            } else {
                changeCalled++;
                if (turn.equals(com.kente_factory.dami.enumerations.Player.white)) {
                    turn = com.kente_factory.dami.enumerations.Player.black;
                } else {
                    turn = com.kente_factory.dami.enumerations.Player.white;
                }
            }

            updateAllTouchable(false);

        }

        if (matchType.equals(com.kente_factory.dami.enumerations.MatchType.online)) {
//            first, play your turn
            changeCalled++;
            if (turn.equals(com.kente_factory.dami.enumerations.Player.white)) {
                turn = com.kente_factory.dami.enumerations.Player.black;
            } else {
                turn = com.kente_factory.dami.enumerations.Player.white;
            }
//            put it in variables
            Utils.storeMovesTurnOnlineGameLocal(turn, current_moves);
//            then use those to update the game state on GPG by taking a turn
            game.getPlatformInterface().updateOnlineMatch(false);

//            if the match is over, end the game locally
            if (board.CheckGameComplete() || board.CheckGameDraw(Board.getOtherTurn(turn))) {
                locallyProcessEndGame();
            }

            updateAllTouchable(false);

        }
        current_moves.clear();
    }

    private void locallyProcessEndGame(){
        game_over = true;

//            put it in variables
//        Utils.storeMovesTurnOnlineGameLocal(Utils.getOtherTurn(turn), current_moves);
//            if after initializing you realize the game is over, don't update the match
//        if(from_initialize) {
//            game.getPlatformInterface().updateOnlineMatch(true);
//        }

//                pick which message depending on draw or someone won
        if (board.CheckGameComplete()) {
            HUD.addActor(new com.kente_factory.dami.GameMessage(com.kente_factory.dami.enumerations.MatchType.online, game_options, Utils.getOtherTurn(turn), game));
        } else {
            HUD.addActor(new GameMessage(com.kente_factory.dami.enumerations.MatchType.online, game_options, null, game));
        }

//        give it some time before you end the game
//        new Timer().scheduleTask(new Timer.Task() {
//            @Override
//            public void run() {
//                game.getPlatformInterface().endOnlineMatch();
//            }
//        }, 1f);
        Gdx.input.setInputProcessor(HUD);
    }

    public void playAturn() {

        Move move = null;
        if (!isComputerTurn()) {
            move = new Move(user_origin[0], user_origin[1], user_destination[0], user_destination[1]);
        }

        if (turn.equals(com.kente_factory.dami.enumerations.Player.white)) {
            if (whiteOwner.equals(Owner.HUMAN)) {
                if (Human.CheckValidMoveForWhiteHuman(move.initialRow, move.initialCol,
                        move.finalRow, move.finalCol)) {
                    GameScreen.board.genericMakeWhiteMove(move);
                    setPlaying(true);
                    current_moves.add(move);
                }
            } else {
                robotCallOnBackground(com.kente_factory.dami.enumerations.Player.white);
            }
        } else if (turn.equals(com.kente_factory.dami.enumerations.Player.black)) {
            if (blackOwner.equals(Owner.HUMAN)) {
                if (Human.CheckValidMoveForBlackHuman(move.initialRow, move.initialCol,
                        move.finalRow, move.finalCol)) {
                    GameScreen.board.genericMakeBlackMove(move);
                    setPlaying(true);
                    current_moves.add(move);
                }
            } else {
                robotCallOnBackground(com.kente_factory.dami.enumerations.Player.black);
            }
        }
    }

    // robot thinks about his next move and plays in background
    private static void robotCallOnBackground(final com.kente_factory.dami.enumerations.Player player) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                // do something important here, asynchronously to the rendering thread
                if (player.equals(com.kente_factory.dami.enumerations.Player.white)) {
                    Robot.makeNextWhiteMoves();
                } else
                    Robot.makeNextBlackMoves();
                // post a Runnable to the rendering thread that processes the result
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        // process the result, e.g. add it to an Array<Result> field of the ApplicationListener.

                    }
                });
            }
        }).start();
    }

    public void updateAllTouchable(boolean all_disabled) {
        for (Actor actor : stage.getActors()) {
            if (actor instanceof Piece) {
                if (matchType.equals(MatchType.online)) {
                    ((Piece) actor).updateTouchableOnline(all_disabled);
                } else {
                    ((Piece) actor).updateTouchable(all_disabled);
                }
            }
        }
    }

    private void rotateAllPieces() {
        for (Actor actor : stage.getActors()) {
            if (actor instanceof Piece) {
                ((Piece) actor).setRotateFactor(-1);
            }
        }
    }
    // callback for when a player is done playing

    public void setPlaying(boolean playing) {
        GameScreen.playing = playing;
        // no one should be touchable while a move is occurring
        if (GameScreen.playing) {
            updateAllTouchable(true);
        }
        if (!GameScreen.playing) {
            changeTurn();
            // if play is done prevent another move from occurring
            user_origin = null;
            user_destination = null;
            if (isComputerTurn() && !game_over) {
                playAturn();
            }
        }
    }

    public boolean isComputerTurn() {
        return turn.equals(com.kente_factory.dami.enumerations.Player.white) && whiteOwner.equals(Owner.ROBOT) || turn.equals(Player.black) && blackOwner.equals(Owner.ROBOT);
    }

    @Override
    public void show() {
        Dami.stage_type = Dami.STAGE_TYPE.match;

        InputMultiplexer inputMultiplexer = new InputMultiplexer(stage, HUD);
        Gdx.input.setInputProcessor(inputMultiplexer);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255f, 255f, 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        stage.act();
        stage.draw();
        HUD.act();
        HUD.draw();
        game.batch.end();

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        Assets.reset();
    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {

    }
}
