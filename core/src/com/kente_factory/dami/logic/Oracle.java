package com.kente_factory.dami.logic;

import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.enumerations.Player;

/**
 *
 * @author apurv and WAHIB
 */
public class Oracle {

    public final int POINT_WON = 100000;
    public final int POINT_KING = 2000;
    public final int POINT_NORMAL = 1000;
    public final int POINT_CENTRAL_PIECE = 100;
    public final int POINT_END_PIECE = 50;
    public final int POINT_DEFENCE = 50;
    public final int POINT_ATTACK_NORMAL = 30;
    public final int POINT_ATTACK_KING = 60;

    public int evaluateBoard(Board board, com.kente_factory.dami.enumerations.Player player) {
        int boardValue = 0;

        if (player == Player.black) {
            boardValue = evaluateValueofBoardForBlack(board);
        } else {
            boardValue = evaluateValueofBoardForWhite(board);
        }

        return boardValue;
    }

    private int evaluateValueofBoardForWhite(Board board) {

        int wValue = 0;
        if (board.isWhiteWinner()) {
            wValue += POINT_WON;
            return wValue;
        } else {
            wValue = WhiteBlackPiecesDifferencePoints(board);
//            wValue += BoardPositionPoints(board);
            wValue /= board.blackPieces;
        }

        return wValue;
    }

    private int evaluateValueofBoardForBlack(Board board) {

        int bValue = 0;
        if (board.isBlackWinner()) {
            bValue -= POINT_WON;
            return bValue;
        } else {
            bValue = WhiteBlackPiecesDifferencePoints(board);
//            bValue += BoardPositionPoints(board);
            bValue /= board.whitePieces;
        }

        return bValue;
    }

    private int WhiteBlackPiecesDifferencePoints(Board board) {
        
        int value = 0;
        // Scan across the board
        for (int r = 0; r < Board.rows; r++) {
            // Check only valid cols
            int c = (r % 2 == 0) ? 0 : 1;
            for (; c < Board.cols; c += 2) {
                assert (!board.cell_entries[r][c].equals(com.kente_factory.dami.enumerations.CellEntry.inValid));
                com.kente_factory.dami.enumerations.CellEntry entry = board.cell_entries[r][c];

                if (entry == com.kente_factory.dami.enumerations.CellEntry.white) {
                    value += POINT_NORMAL;
                } else if (entry == com.kente_factory.dami.enumerations.CellEntry.whiteKing) {
                    value += POINT_KING;
                } else if (entry == com.kente_factory.dami.enumerations.CellEntry.black) {
                    value -= POINT_NORMAL;
                } else if (entry == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                    value -= POINT_KING;
                }
            }
        }
        return value;
    }

    private int BoardPositionPoints(Board board) {

        int value = 0;
        
        // Central Points
        if (board.cell_entries[3][3] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[3][3] == com.kente_factory.dami.enumerations.CellEntry.whiteKing
                || board.cell_entries[3][5] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[3][5] == com.kente_factory.dami.enumerations.CellEntry.whiteKing) {
            value += POINT_CENTRAL_PIECE;
        }
        if (board.cell_entries[4][2] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[4][2] == com.kente_factory.dami.enumerations.CellEntry.blackKing
                || board.cell_entries[4][4] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[4][4] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
            value -= POINT_CENTRAL_PIECE;
        }
        
        // End Points
        if (board.cell_entries[0][2] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[0][4] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[0][6] == com.kente_factory.dami.enumerations.CellEntry.white){
            value += POINT_END_PIECE;
        }
        
        if (board.cell_entries[7][1] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[7][3] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[7][5] == com.kente_factory.dami.enumerations.CellEntry.black){
            value -= POINT_END_PIECE;
        }
                
        return value;
    }

    //Calculate points for attacking white PIECE.
    //To be called only by on a cell_entries which has a black PIECE.
    private int calcPointsFAforBlack(Board board, int r, int c) {

        com.kente_factory.dami.enumerations.CellEntry entry = board.cell_entries[r][c];
        assert ((entry == com.kente_factory.dami.enumerations.CellEntry.black || entry == com.kente_factory.dami.enumerations.CellEntry.blackKing));
        int points = 0;

        if (r > 0) {
            if (c < Board.cols - 1) {
                if ((board.cell_entries[r - 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[r - 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)
                        && r < Board.rows - 1 && c >= 1 && board.cell_entries[r + 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.empty) {
                    points -= POINT_ATTACK_NORMAL;
                }
                //TODO: Add backward attack for black king.

            }

            if (c > 0) {
                //Attack Points
                if ((board.cell_entries[r - 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[r - 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.whiteKing)
                        && r < Board.rows - 1 && c < Board.cols - 1 && board.cell_entries[r + 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.empty) {
                    points -= POINT_ATTACK_NORMAL;
                }
            }
        }
        return points;
    }

    //Calculate points for defending black.
    private int calcPointsFDforBlack(Board board, int r, int c) {

        com.kente_factory.dami.enumerations.CellEntry entry = board.cell_entries[r][c];
        assert ((entry == com.kente_factory.dami.enumerations.CellEntry.black || entry == com.kente_factory.dami.enumerations.CellEntry.blackKing));
        int points = 0;

        if (r == Board.rows - 1) {
            points += POINT_DEFENCE;
        } else if (r == Board.rows - 3
                && ((c < Board.cols - 2 && board.cell_entries[Board.rows - 1][c + 2] == com.kente_factory.dami.enumerations.CellEntry.black && board.cell_entries[Board.rows - 2][c + 1] == com.kente_factory.dami.enumerations.CellEntry.empty)
                || (c >= 2 && board.cell_entries[Board.rows - 1][c - 2] == com.kente_factory.dami.enumerations.CellEntry.black && board.cell_entries[Board.rows - 2][c - 1] == com.kente_factory.dami.enumerations.CellEntry.empty))) {
            points += POINT_DEFENCE;
        } else if (r < Board.rows - 1) {

            if (c < Board.cols - 1) {
                if (board.cell_entries[r + 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[r + 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                    points += POINT_DEFENCE;
                }

            }

            if (c > 0) {
                //Defense points
                if (board.cell_entries[r + 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[r + 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                    points += POINT_DEFENCE;
                }
            }

        }
        return points;
    }

    private int calcPointsFAforWhite(Board board, int r, int c) {
        com.kente_factory.dami.enumerations.CellEntry entry = board.cell_entries[r][c];
        assert ((entry == com.kente_factory.dami.enumerations.CellEntry.white || entry == com.kente_factory.dami.enumerations.CellEntry.whiteKing));
        int points = 0;

        if (r < Board.rows - 1) {
            if (c < Board.cols - 1) {

                //Debit points for unsafe attack.
                if (board.cell_entries[r + 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[r + 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                    points -= POINT_ATTACK_NORMAL;
                }

            }
            if (c > 0) {

                //Attack Points.
                if (board.cell_entries[r + 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.black || board.cell_entries[r + 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.blackKing) {
                    points += POINT_ATTACK_NORMAL;
                }

            }
        }

        return points;
    }

    private int calcPointsFDforWhite(Board board, int r, int c) {

        com.kente_factory.dami.enumerations.CellEntry entry = board.cell_entries[r][c];
        assert ((entry == com.kente_factory.dami.enumerations.CellEntry.white || entry == com.kente_factory.dami.enumerations.CellEntry.whiteKing));
        int points = 0;

        if (r == 0) {
            points += POINT_DEFENCE;
        } else if (r == 2
                && ((c < Board.cols - 2 && board.cell_entries[2][c + 2] == com.kente_factory.dami.enumerations.CellEntry.white && board.cell_entries[1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.empty)
                || (c >= 2 && board.cell_entries[0][c - 2] == com.kente_factory.dami.enumerations.CellEntry.black && board.cell_entries[1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.empty))) {
            points += POINT_DEFENCE;
        } else if (r >= 2) {
            if (c < Board.cols - 1) {

                //Defense Points
                if (board.cell_entries[r - 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[r - 1][c + 1] == com.kente_factory.dami.enumerations.CellEntry.whiteKing) {
                    points += POINT_DEFENCE;
                }
            }
            if (c > 0) {

                if (board.cell_entries[r - 1][c - 1] == com.kente_factory.dami.enumerations.CellEntry.white || board.cell_entries[r - 1][c - 1] == CellEntry.whiteKing) {
                    points += POINT_DEFENCE;
                }
            }
        }

        return points;
    }
}