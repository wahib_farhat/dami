package com.kente_factory.dami;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Dami extends Game {

    public SpriteBatch batch;

	public enum STAGE_TYPE {menu, match, store}

	public static STAGE_TYPE stage_type;

	public static MainMenuScreen menuScreen;

	private PlatformInterface platformInterface;

	public static StoreItem circlePiece, diamondPiece, squarePiece, trianglePiece;
	public static StoreItem normalCrown, queenCrown, kingCrown, hatCrown;

	public static StoreItemSelector circlePieceSelector;
	public static StoreItemSelector diamondPieceSelector;
	public static StoreItemSelector squarePieceSelector;
	public static StoreItemSelector trianglePieceSelector;

	public static StoreItemSelector normalCrownSelector;
	public static StoreItemSelector queenCrownSelector;
	public static StoreItemSelector kingCrownSelector;
	public static StoreItemSelector bowlingHatCrownSelector;

	public Dami() {

    }

	public Dami(PlatformInterface platformInterface) {
        this.platformInterface = platformInterface;
	}

	@Override
	public void create () {


        Utils.initialize();

//        localdata loaded after assets have been loaded
	    OnlineUserData.initialize();

		batch = new SpriteBatch();
        this.setScreen(new LoadingScreen(this));

	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
	}

    public PlatformInterface getPlatformInterface() {
        return platformInterface;
    }
}
