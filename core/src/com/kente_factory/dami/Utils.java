package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.enumerations.Player;
import com.kente_factory.dami.logic.BoardItems;
import com.kente_factory.dami.logic.GameScreen;
import com.kente_factory.dami.logic.Move;

import java.util.ArrayList;

public class Utils {

    private static int last_music;

    public static Json json_instance;

    private static CellEntry[][] default_board = new CellEntry[][]{
            {CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white},
            {CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white, CellEntry.inValid, CellEntry.white},
            {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
            {CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black},
            {CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.black},
    };
    private static CellEntry[][] testing_board = new CellEntry[][]{
            {CellEntry.white, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
            {CellEntry.empty, CellEntry.inValid, CellEntry.black, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
            {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
            {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
            {CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid},
            {CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty, CellEntry.inValid, CellEntry.empty},
    };

    public static BoardItems boardItems = new BoardItems();
    public static Texture game_message_backgroundTexture;
    public static Texture dark_bgTexture;

    public static void initialize() {
        Utils.json_instance = new Json();

        Pixmap dark_bg = new Pixmap(1080, 1920, Pixmap.Format.RGBA8888);
        dark_bg.setColor(new Color(0, 0, 0, 0.5f));
        dark_bg.fill();

        Pixmap game_message_background = new Pixmap(965, 965, Pixmap.Format.RGBA8888);
        game_message_background.setColor(new Color(196f / 225, 154f / 225, 108f / 225, 1));
        game_message_background.fill();

        game_message_backgroundTexture = new Texture(game_message_background);
        dark_bgTexture = new Texture(dark_bg);
    }

    public static void saveLocalGame() {
//        LocalUserData.updateLocalBoard(GameScreen.board.cell_entries);
//        LocalUserData.updateLocalOwnedPlayer(getPlayerFromOptions(GameScreen.game_options));
//        LocalUserData.updateLocalCurrentPlayer(GameScreen.turn);
    }

    private static Player getPlayerFromOptions(GameScreen.GAME_OPTIONS game_options) {
        switch (game_options) {
            case white:
                return Player.white;
            case black:
                return Player.black;
            default:
//          something is wrong
                return null;
        }
    }

    //    update the json strings that will be used in updating the game state
    public static void storeMovesTurnOnlineGameLocal(Player current_player, ArrayList<Move> moves) {
        Gdx.app.log("Dami", "raw moves: " + json_instance.toJson(moves));
        OnlineUserData.updateOnlineCurrentPlayerLocal(current_player.toString());
        OnlineUserData.updateOnlineCurrentMovesLocal(json_instance.toJson(moves));
    }

    public static void storeBoardOnlineGameLocal(CellEntry[][] cell_entries_json) {
        Gdx.app.log("Dami", "raw board: " + json_instance.toJson(cell_entries_json));
        OnlineUserData.updateOnlineBoardLocal(json_instance.toJson(cell_entries_json));
    }

    public static void updateLocalFromGPG(String json) {
        CellEntry[][] cellEntries = getBoardFromJson(json);
        GameScreen.restoreFromTurnData(cellEntries);
    }

    public static GameScreen.GAME_OPTIONS getOptionsFromPlayer(Player player) {
        switch (player) {
            case white:
                return GameScreen.GAME_OPTIONS.white;
            case black:
                return GameScreen.GAME_OPTIONS.black;
            default:
//          something is wrong
                return null;
        }
    }

    public static int getWhitePiecesFromBoardData(CellEntry[][] cells) {

        int totalWhitePieces = 0;

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].equals(CellEntry.white) || cells[i][j].equals(CellEntry.whiteKing)) {
                    totalWhitePieces++;
                }
            }
        }

        return totalWhitePieces;
    }

    public static int getBlackPiecesFromBoardData(CellEntry[][] cells) {

        int totalBlackPieces = 0;

        for (int i = 0; i < cells.length; i++) {
            for (int j = 0; j < cells[i].length; j++) {
                if (cells[i][j].equals(CellEntry.black) || cells[i][j].equals(CellEntry.blackKing)) {
                    totalBlackPieces++;
                }
            }
        }

        return totalBlackPieces;
    }

    public static CellEntry getEntry(String entry_name) {

        if (entry_name.equals("white")) {
            return CellEntry.white;
        }
        if (entry_name.equals("black")) {
            return CellEntry.black;
        }

        if (entry_name.equals("whiteKing")) {
            return CellEntry.whiteKing;
        }
        if (entry_name.equals("blackKing")) {
            return CellEntry.blackKing;
        }

        if (entry_name.equals("empty")) {
            return CellEntry.empty;
        }
        if (entry_name.equals("inValid")) {
            return CellEntry.inValid;
        }

//        something is wrong
        return CellEntry.inValid;
    }

    public static Player getPlayerFromString(String playerString) {
        if (playerString.equalsIgnoreCase("white")) {
            return Player.white;
        }
        if (playerString.equalsIgnoreCase("black")) {
            return Player.black;
        }

//        something is wrong
        Gdx.app.log("DAMI", "ERROR IN getPlayerFromString. string was: " + playerString);
        return null;
    }

    public static CellEntry[][] getBoardFromJson(String json) {

        Gdx.app.log("DamiAndroid", json);

        Array<Array<String>> entries_string = json_instance.fromJson(null, json);

        CellEntry[][] board = new CellEntry[10][10];

        for (int i = 0; i < entries_string.size; i++) {

            for (int j = 0; j < entries_string.get(i).size; j++) {
                board[i][j] = getEntry(entries_string.get(i).get(j));
            }

        }

        return board;
    }


    public static ArrayList<Move> getMovesFromJson(final String json_string) {

        Gdx.app.log("DamiAndroidMoves", json_string);

        Json json = new Json();
        json.setSerializer(Move.class, new Json.Serializer<Move>() {
            @Override
            public void write(Json json, Move object, Class knownType) {

            }

            @Override
            public Move read(Json json, JsonValue jsonData, Class type) {
                return new Move(jsonData.getInt("initialRow"), jsonData.getInt("initialCol"), jsonData.getInt("finalRow"), jsonData.getInt("finalCol"));
            }
        });

        ArrayList moves = json.fromJson(ArrayList.class, json_string);

//        Gdx.app.log("DamiAndroidMoves", moves.toString());


        for (Object move : moves) {

            Gdx.app.log("DamiAndroidMoves", move + "");
//            Gdx.app.log("DamiAndroidMoves", move.initialRow + " " + move.initialCol + " " + move.finalRow + " " + move.finalCol);

        }

        return moves;
    }

    //    utility method for returning the opponent's turn
    public static Player getOtherTurn(Player turn) {
        if (turn.equals(Player.white)) {
            return Player.black;
        }
        return Player.white;
    }

    public static CellEntry[][] getDefaultBoard() {
        return default_board;
    }

    /**
     * get an int between two numbers
     *
     * @param a first number
     * @param b second number
     * @return
     */
    public static int getAnIntFrom(int a, int b) {
        return (int) (a + (Math.random() * ((b + 1) - a)));
    }

    private static int getNewInt() {
        int randomizer = Utils.getAnIntFrom(1, 2);

        if (randomizer != last_music) {
            last_music = randomizer;
            return randomizer;
        } else {
            getNewInt();
        }
        return randomizer;
    }

    private static void playMusic(Music... musics) {
        if (LocalUserData.sound_enabled) {
        // play each music object if it's not already playing
        for (Music music_param : musics) {
            if (!music_param.isPlaying()) {
                music_param.play();
            }
        }
        }
    }

    public static void pauseGameMusic() {

        Music[] musics = new Music[] {Assets.game_music1, Assets.game_music2};

        for(Music music : musics) {
            if (music.isPlaying()) {
                music.pause();
            }
        }

    }

    public static void playGameMusic() {
        if (!Assets.game_music1.isPlaying() &&
                !Assets.game_music1.isPlaying()) {
            int randomizer = getNewInt();

            switch (randomizer) {
                case 1:
                    playMusic(Assets.game_music1);
                    break;
                case 2:
                    playMusic(Assets.game_music2);
                    break;
            }
        }
    }
}
