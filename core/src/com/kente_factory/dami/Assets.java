package com.kente_factory.dami;

/*
 * Created by wahib on 12/27/2017.
 */

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Assets {
    private static Assets INSTANCE;

    public AssetManager assetManager = new AssetManager();

    public static Music game_music1;
    public static Music game_music2;

    public static Sound move_piece_sound;
    public static Sound capture_piece_sound;

    public static Texture circle_black_pieceTexture;
    public static Texture circle_white_pieceTexture;

    public static Texture square_black_pieceTexture;
    public static Texture square_white_pieceTexture;

    public static Texture diamond_black_pieceTexture;
    public static Texture diamond_white_pieceTexture;

    public static Texture triangle_black_pieceTexture;
    public static Texture triangle_white_pieceTexture;

//    crowns
    public static Texture crown_blackTexture;
    public static Texture crown_whiteTexture;
    public static Texture crown_queenTexture;
    public static Texture crown_kingTexture;
    public static Texture crown_bowling_hatTexture;

    public static Texture board_backTexture;
    public static Texture board_edgeTexture;
    public static Texture black_boxTexture;
    public static Texture icon_selectTexture;

    public static Texture mainmenu_bgTexture;
    public static Texture one_player_buttonTexture;
    public static Texture two_player_buttonTexture;
    public static Texture games_buttonTexture;
    public static Texture signin_buttonTexture;
    public static Texture signout_buttonTexture;
    public static Texture store_buttonTexture;
    public static Texture loading_bgTexture;
    public static Texture loading_spinnerTexture;

    public static Texture white_buttonTexture;
    public static Texture black_buttonTexture;


    public static Texture yes_buttonTexture;
    public static Texture no_buttonTexture;

    public static Texture sound_buttonTexture;

    public static Texture player_won_messageTexture;
    public static Texture player_lost_messageTexture;
    public static Texture white_won_messageTexture;
    public static Texture black_won_messageTexture;
    public static Texture quit_game_messageTexture;
    public static Texture exit_game_messageTexture;
    public static Texture draw_messageTexture;

    public static Texture back_buttonTexture;

//    store textures
    public static Texture store_bgTexture;
    public static Texture coins_bgTexture;
    public static Texture store_item_selectorTexture;
    public static Texture piece_bgTexture;
    public static Texture crown_bgTexture;
    public static Texture correct_symbolTexture;
    public static Texture selector_coinTexture;

    public static String fontLocation;


    private Assets() {
        // unused constructor
    }

    /**
     * singleton
     *
     * @return singleton instance of Assets class
     */
    public synchronized static Assets getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Assets();

            move_piece_sound = Gdx.audio.newSound(Gdx.files.
                    internal("sounds/move_piece.wav"));
            capture_piece_sound = Gdx.audio.newSound(Gdx.files.
                    internal("sounds/capture_piece.wav"));

            game_music1 = Gdx.audio.newMusic(Gdx.files
                    .internal("music/game1.mp3"));
            game_music2 = Gdx.audio.newMusic(Gdx.files
                    .internal("music/game2.mp3"));


            game_music1.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music music) {
                    Utils.playGameMusic();
                }
            });
            game_music2.setOnCompletionListener(new Music.OnCompletionListener() {
                @Override
                public void onCompletion(Music music) {
                    Utils.playGameMusic();
                }
            });

            fontLocation = "fonts/nyala.fnt";
        }
        return INSTANCE;
    }

    public static void reset() {
        INSTANCE = null;
    }

    /**
     * loads all resources
     */
    public synchronized void loadResources() {

//  texture for all the pieces
        assetManager.load("pieces/circle_white.png",
                Texture.class);
        assetManager.load("pieces/circle_black.png",
                Texture.class);
        assetManager.load("pieces/square_white.png",
                Texture.class);
        assetManager.load("pieces/square_black.png",
                Texture.class);
        assetManager.load("pieces/triangle_white.png",
                Texture.class);
        assetManager.load("pieces/triangle_black.png",
                Texture.class);
        assetManager.load("pieces/diamond_white.png",
                Texture.class);
        assetManager.load("pieces/diamond_black.png",
                Texture.class);
        assetManager.load("pieces/crown_white.png",
                Texture.class);
        assetManager.load("pieces/crown_black.png",
                Texture.class);
        assetManager.load("pieces/crown_queen.png",
                Texture.class);
        assetManager.load("pieces/crown_king.png",
                Texture.class);
        assetManager.load("pieces/crown_bowling_hat.png",
                Texture.class);


        assetManager.load("board2.png",
                Texture.class);
        assetManager.load("board_edge.png",
                Texture.class);
        assetManager.load("black_box.png",
                Texture.class);
        assetManager.load("icon_select.png",
                Texture.class);

//        main menu textures
        assetManager.load("mainmenu_bg.png",
                Texture.class);
        assetManager.load("one_player_button.png",
                Texture.class);
        assetManager.load("two_player_button.png",
                Texture.class);
        assetManager.load("white_button.png",
                Texture.class);
        assetManager.load("black_button.png",
                Texture.class);
        assetManager.load("games_button.png",
                Texture.class);
        assetManager.load("signin_button.png",
                Texture.class);
        assetManager.load("signout_button.png",
                Texture.class);
        assetManager.load("sound_button.png",
                Texture.class);
        assetManager.load("store_button.png",
                Texture.class);
        assetManager.load("loading_bg.png",
                Texture.class);
        assetManager.load("loading_spinner.png",
                Texture.class);

        assetManager.load("lost_message.png",
                Texture.class);
        assetManager.load("won_message.png",
                Texture.class);
        assetManager.load("white_won_message.png",
                Texture.class);
        assetManager.load("black_won_message.png",
                Texture.class);
        assetManager.load("quit_game_message.png",
                Texture.class);
        assetManager.load("exit_game_message.png",
                Texture.class);
        assetManager.load("draw_message.png",
                Texture.class);
        assetManager.load("yes_button.png",
                Texture.class);
        assetManager.load("no_button.png",
                Texture.class);

        assetManager.load("back_button.png",
                Texture.class);

//        store textures
        assetManager.load("store/store_bg.png",
                Texture.class);
        assetManager.load("store/coins_bg.png",
                Texture.class);
        assetManager.load("store/store_item_selector.png",
                Texture.class);
        assetManager.load("store/piece_bg.png",
                Texture.class);
        assetManager.load("store/crown_bg.png",
                Texture.class);
        assetManager.load("store/correct_symbol.png",
                Texture.class);
        assetManager.load("store/selector_coin.png",
                Texture.class);

    }

    public synchronized void assignResources() {
        board_backTexture = Assets.getInstance().assetManager.get("board2.png", Texture.class);
        board_edgeTexture = Assets.getInstance().assetManager.get("board_edge.png", Texture.class);
        black_boxTexture = Assets.getInstance().assetManager.get("black_box.png", Texture.class);

//        pieces
        circle_white_pieceTexture = Assets.getInstance().assetManager.get("pieces/circle_white.png", Texture.class);
        circle_black_pieceTexture = Assets.getInstance().assetManager.get("pieces/circle_black.png", Texture.class);
        square_white_pieceTexture = Assets.getInstance().assetManager.get("pieces/square_white.png", Texture.class);
        square_black_pieceTexture = Assets.getInstance().assetManager.get("pieces/square_black.png", Texture.class);
        diamond_white_pieceTexture = Assets.getInstance().assetManager.get("pieces/diamond_white.png", Texture.class);
        diamond_black_pieceTexture = Assets.getInstance().assetManager.get("pieces/diamond_black.png", Texture.class);
        triangle_white_pieceTexture = Assets.getInstance().assetManager.get("pieces/triangle_white.png", Texture.class);
        triangle_black_pieceTexture = Assets.getInstance().assetManager.get("pieces/triangle_black.png", Texture.class);

        icon_selectTexture = Assets.getInstance().assetManager.get("icon_select.png", Texture.class);

        crown_blackTexture = Assets.getInstance().assetManager.get("pieces/crown_black.png", Texture.class);
        crown_whiteTexture = Assets.getInstance().assetManager.get("pieces/crown_white.png", Texture.class);
        crown_queenTexture = Assets.getInstance().assetManager.get("pieces/crown_queen.png", Texture.class);
        crown_kingTexture = Assets.getInstance().assetManager.get("pieces/crown_king.png", Texture.class);
        crown_bowling_hatTexture = Assets.getInstance().assetManager.get("pieces/crown_bowling_hat.png", Texture.class);

//        main menu textures
        mainmenu_bgTexture = Assets.getInstance().assetManager.get("mainmenu_bg.png", Texture.class);
        one_player_buttonTexture = Assets.getInstance().assetManager.get("one_player_button.png", Texture.class);
        two_player_buttonTexture = Assets.getInstance().assetManager.get("two_player_button.png", Texture.class);
        white_buttonTexture = Assets.getInstance().assetManager.get("white_button.png", Texture.class);
        black_buttonTexture = Assets.getInstance().assetManager.get("black_button.png", Texture.class);
        store_buttonTexture = Assets.getInstance().assetManager.get("store_button.png", Texture.class);
        games_buttonTexture = Assets.getInstance().assetManager.get("games_button.png", Texture.class);
        signin_buttonTexture = Assets.getInstance().assetManager.get("signin_button.png", Texture.class);
        signout_buttonTexture = Assets.getInstance().assetManager.get("signout_button.png", Texture.class);
        sound_buttonTexture = Assets.getInstance().assetManager.get("sound_button.png", Texture.class);
        loading_bgTexture = Assets.getInstance().assetManager.get("loading_bg.png", Texture.class);
        loading_spinnerTexture = Assets.getInstance().assetManager.get("loading_spinner.png", Texture.class);

        player_lost_messageTexture = Assets.getInstance().assetManager.get("lost_message.png", Texture.class);
        player_won_messageTexture = Assets.getInstance().assetManager.get("won_message.png", Texture.class);
        black_won_messageTexture = Assets.getInstance().assetManager.get("black_won_message.png", Texture.class);
        white_won_messageTexture = Assets.getInstance().assetManager.get("white_won_message.png", Texture.class);
        quit_game_messageTexture = Assets.getInstance().assetManager.get("quit_game_message.png", Texture.class);
        exit_game_messageTexture = Assets.getInstance().assetManager.get("exit_game_message.png", Texture.class);
        draw_messageTexture = Assets.getInstance().assetManager.get("draw_message.png", Texture.class);
        yes_buttonTexture = Assets.getInstance().assetManager.get("yes_button.png", Texture.class);
        no_buttonTexture = Assets.getInstance().assetManager.get("no_button.png", Texture.class);

        back_buttonTexture = Assets.getInstance().assetManager.get("back_button.png", Texture.class);

//        store textures
        store_bgTexture = Assets.getInstance().assetManager.get("store/store_bg.png", Texture.class);
        coins_bgTexture = Assets.getInstance().assetManager.get("store/coins_bg.png", Texture.class);

        store_item_selectorTexture = Assets.getInstance().assetManager.get("store/store_item_selector.png", Texture.class);
        piece_bgTexture = Assets.getInstance().assetManager.get("store/piece_bg.png", Texture.class);
        crown_bgTexture = Assets.getInstance().assetManager.get("store/crown_bg.png", Texture.class);
        correct_symbolTexture = Assets.getInstance().assetManager.get("store/correct_symbol.png", Texture.class);
        selector_coinTexture = Assets.getInstance().assetManager.get("store/selector_coin.png", Texture.class);


    }
}

