package com.kente_factory.dami;


import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class BackButton extends Button {

    public BackButton(final ButtonAction buttonAction) {
        super(Assets.back_buttonTexture, 15, 880, buttonAction);
    }
}
