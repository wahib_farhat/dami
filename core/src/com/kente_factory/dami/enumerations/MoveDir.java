package com.kente_factory.dami.enumerations;

public enum MoveDir{
    forwardLeft,
    forwardRight,
    backwardLeft,
    backwardRight
}
