package com.kente_factory.dami.enumerations;

/**
 *
 * @author WAHIB
 */
public enum CellEntry{
    inValid,
    empty,
    white,
    whiteKing,
    black,
    blackKing
}
