package com.kente_factory.dami.enumerations.store_item_types;

public enum CrownType {
    normal,
    queen,
    king,
    hat
}
