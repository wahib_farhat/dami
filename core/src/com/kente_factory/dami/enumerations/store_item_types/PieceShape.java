package com.kente_factory.dami.enumerations.store_item_types;

public enum PieceShape {
    circle,
    square,
    triangle,
    diamond
}
