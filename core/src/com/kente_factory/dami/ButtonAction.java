package com.kente_factory.dami;

public interface ButtonAction {

    void onClick();
}
