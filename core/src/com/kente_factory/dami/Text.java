package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;

public class Text extends Actor {

    public BitmapFont font;
    CharSequence letters;
    float x, y;
    float wrappedWidth;

    boolean center;


    private GlyphLayout glyphLayout;

    /**
     * a PIECE of text that will not be automatically added to the stage
     *
     * @param letters
     * @param x
     * @param y
     * @param wrappedWidth
     */
    public Text(CharSequence letters, float x, float y, float scale, Color color,
                float wrappedWidth, boolean center) {
        this.font = new BitmapFont(Gdx.files.internal(Assets.fontLocation));
        font.getData().setScale(scale);
        this.letters = letters;
        this.wrappedWidth = wrappedWidth;
        this.setColor(color);
        this.setPosition(x, y);
        this.x = x;
        this.y = y;

        this.center = center;

        glyphLayout = new GlyphLayout();

    }

    /**
     * a PIECE of text that will be automatically added to the stage
     *
     * @param letters
     * @param x
     * @param y
     * @param wrappedWidth
     * @param stage
     */
    public Text(CharSequence letters, float x, float y, float scale, Color color,
                float wrappedWidth, Stage stage, boolean center) {
        this.font = new BitmapFont(Gdx.files.internal(Assets.fontLocation));
        font.getData().setScale(scale);
        this.letters = letters;
        this.wrappedWidth = wrappedWidth;
        this.setColor(color);
        this.setPosition(x, y);
        this.x = x;
        this.y = y;

        this.center = center;

        glyphLayout = new GlyphLayout();
        stage.addActor(this);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        font.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        int alignment = center ? Align.center : Align.left;

        glyphLayout.setText(font, letters, font.getColor(), wrappedWidth, alignment, true);

        font.draw(batch, letters, x, y, wrappedWidth, alignment, true);

//        if (center) {
//            font.draw(batch, glyphLayout, this.getX() - this.glyphLayout.width / 2, this.getY() + this.glyphLayout.height / 2);
//        } else if (wrappedWidth > 0) {
//            font.draw(batch, letters, this.getX(), this.getY(), wrappedWidth, 10, true);
//        } else {
//            font.draw(batch, letters, this.getX(), this.getY());
//        }
        super.draw(batch, parentAlpha);
    }

    public void setScale(float scale){
        this.font.getData().setScale(scale);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    public CharSequence getLetters() {
        return this.letters;
    }

    public void setLetters(CharSequence letters) {
        this.letters = letters;
    }
}
