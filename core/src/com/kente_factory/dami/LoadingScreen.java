package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kente_factory.dami.enumerations.store_item_types.CrownType;
import com.kente_factory.dami.enumerations.store_item_types.PieceShape;

/**
 * Created by wahib on 12/27/2017.
 */

public class LoadingScreen implements Screen {

    private final Stage stage;
    private final Dami game;

    public LoadingScreen(Dami game){
        stage = new Stage(new StretchViewport(540, 960));
        this.game = game;

        Assets.getInstance().loadResources();
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255f, 255f, 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        stage.act();
        stage.draw();

        if (Assets.getInstance().assetManager.update()) {
            // then assign them to textures
            Assets.getInstance().assignResources();

//            game.menu = new MainMenuScreen(game);
            Dami.menuScreen = new MainMenuScreen(game);

            assignGameVariables();

            // set to the main game screen
            this.game.setScreen(Dami.menuScreen);

            assignStoreItemSelectorVariables();
        }

        game.batch.end();
    }

    private void assignGameVariables() {

        Dami.circlePiece = new StoreItem(1, PieceShape.circle,"circle piece", "Playing piece shaped like a circle",
                "Playing piece shaped like a circle", 0, Assets.circle_white_pieceTexture);
        Dami.diamondPiece = new StoreItem(2, PieceShape.diamond,"diamond piece", "Playing piece shaped like a diamond",
                "Playing piece shaped like a diamond", 50, Assets.diamond_white_pieceTexture);
        Dami.squarePiece = new StoreItem(3, PieceShape.square,"square piece", "Playing piece shaped like a square",
                "Playing piece shaped like a square", 100, Assets.square_white_pieceTexture);
        Dami.trianglePiece = new StoreItem(4, PieceShape.triangle,"triangle piece", "Playing piece shaped like a triangle",
                "Playing piece shaped like a triangle", 500, Assets.triangle_white_pieceTexture);



        Dami.normalCrown = new StoreItem(5, CrownType.normal,"normal crown", "Crown piece with another piece",
                "Crown piece with another piece", 0, Assets.circle_white_pieceTexture);
        Dami.queenCrown = new StoreItem(6, CrownType.queen,"queen's crown", "A classic queen's crown",
                "Crown your pieces with a classic queen's crown", 500, Assets.crown_queenTexture);
        Dami.kingCrown = new StoreItem(7, CrownType.king,"king's crown", "A classic king's crown",
                "Crown your pieces with a classic king's crown", 750, Assets.crown_kingTexture);
        Dami.hatCrown = new StoreItem(8, CrownType.hat,"bowler hat", "A stylish bowler hat",
                "Crown your pieces with a stylish bowler hat", 1500, Assets.crown_bowling_hatTexture);

        LocalUserData.initialize();

    }

    private void assignStoreItemSelectorVariables() {
        Dami.circlePieceSelector = new StoreItemSelector(0, 0, Dami.circlePiece);
        Dami.diamondPieceSelector = new StoreItemSelector(0, 0, Dami.diamondPiece);
        Dami.squarePieceSelector = new StoreItemSelector(0, 0, Dami.squarePiece);
        Dami.trianglePieceSelector = new StoreItemSelector(0, 0, Dami.trianglePiece);

        Dami.normalCrownSelector = new StoreItemSelector(0, 0, Dami.normalCrown);
        Dami.queenCrownSelector = new StoreItemSelector(0, 0, Dami.queenCrown);
        Dami.kingCrownSelector = new StoreItemSelector(0, 0, Dami.kingCrown);
        Dami.bowlingHatCrownSelector = new StoreItemSelector(0, 0, Dami.hatCrown);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
