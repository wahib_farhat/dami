package com.kente_factory.dami;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Button extends Entity {

    private ButtonAction buttonAction;

    public Button(Texture texture, float x, float y, final ButtonAction buttonAction) {
        super(texture, x, y);

        this.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                Button.this.addAction(Actions.sequence(Actions.scaleTo(0.75f, 0.75f, 0.1f),
//                        Actions.scaleTo(1f, 1f, 0.1f)));
                Button.this.addAction(Actions.scaleTo(0.75f, 0.75f, 0.1f));
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Button.this.addAction(Actions.scaleTo(1f, 1f, 0.1f));
                buttonAction.onClick();
            }
        });
    }


    /**
     * in extending this constructor, you will have to call setButtonAction after. cannot pass the
     * overriden method in the super()
     * @param texture the texture for the entity
     * @param FRAME_COLS number of columns to split it
     * @param FRAME_ROWS number of rows to split it
     * @param x x position
     * @param y y position
     */
    public Button(Texture texture, int FRAME_COLS, int FRAME_ROWS, float x, float y) {
        super(texture, FRAME_COLS, FRAME_ROWS, x, y);

        this.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                Button.this.addAction(Actions.scaleTo(0.75f, 0.75f, 0.1f));
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Button.this.addAction(Actions.scaleTo(1f, 1f, 0.1f));
                buttonAction.onClick();
            }
        });
    }

    public void setButtonAction(ButtonAction buttonAction) {
        this.buttonAction = buttonAction;
    }
}
