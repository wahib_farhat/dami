package com.kente_factory.dami;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class LoadingSpinner extends Group {

    public LoadingSpinner() {

        Entity bg = new Entity(Assets.loading_bgTexture, 0, 0);
        Entity spinner = new Entity(Assets.loading_spinnerTexture, 250, 500);

        spinner.addAction(Actions.forever(Actions.rotateBy(360, 2, Interpolation.bounce)));

        this.addActor(bg);
        this.addActor(spinner);
    }
}
