package com.kente_factory.dami;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kente_factory.dami.enumerations.store_item_types.CrownType;
import com.kente_factory.dami.enumerations.store_item_types.PieceShape;
import com.kente_factory.dami.enumerations.PopUpScreenButtonOptions;

public class StoreItemSelector extends Group {

    private Text priceText;
    //    coin beside the price
    private Entity coin;
    //    symbol signifying ownership of piece
    private static Entity set_piece_symbol = new Entity(Assets.correct_symbolTexture,
            Assets.store_item_selectorTexture.getWidth() / 4 - Assets.correct_symbolTexture.getWidth() / 4,
            (399f - 42f) / 2);
    //    symbol signifying ownership of piece
    private static Entity set_crown_symbol = new Entity(Assets.correct_symbolTexture,
            Assets.store_item_selectorTexture.getWidth() / 4 - Assets.correct_symbolTexture.getWidth() / 4,
            (399f - 42f) / 2);

    private StoreItem storeItem;

    private PopUpMessage buy_popup;
    private PopUpMessage set_item_popup;
    private PopUpMessage already_set_popup;

    private static StoreItemSelector selectedPieceShape;
    private static StoreItemSelector selectedCrownType;

    //    what will actually show when the item is clicked depends on the state of the item, bought(owned) or set or not at all
    private PopUpMessage actual_popup;

    public StoreItemSelector(float x, float y, final StoreItem storeItem) {

        this.storeItem = storeItem;

//        some Entitys' positions depend on these
        this.setPosition(x, y);
        this.setWidth(Assets.store_item_selectorTexture.getWidth() / 2);
        this.setHeight((Assets.store_item_selectorTexture.getHeight() / 2) + 20);

        Entity store_item_selector = new Entity(Assets.store_item_selectorTexture, 0, 0);

        Texture bg_texture;
        if(storeItem.getItemType() instanceof PieceShape) {
            bg_texture = Assets.piece_bgTexture;
        } else {
            bg_texture = Assets.crown_bgTexture;
        }
        Entity bg_entity = new Entity(bg_texture, 4f / 2, (399f - 290.67f) / 2);
        coin = new Entity(Assets.selector_coinTexture, 69f / 2, (399f - 42f) / 2);

        Entity sample = new Entity(storeItem.getSampleTexture(), 100f / 2, 202f / 2);
        sample.setScale(1.5f);

        priceText = new Text(storeItem.getPrice() + "", 120 / 2, 405 / 2, 0.3f, Color.WHITE, 100, false);
        Text nameText = new Text(storeItem.getName() + "", 50 / 2, 100 / 2, 0.23f, Color.WHITE, 100, true);

        nameText.font.getData().setLineHeight(70f);


//        different strings depending on whether he already owns it or is now buying it
        final String buy_confirmation_message = storeItem.getShort_description() + "\nBuy for " + storeItem.getPrice() + "?";
        final String set_confirmation_message = storeItem.getShort_description() + "\nYou already own this item\nSet it?";
        final String already_set_message = storeItem.getShort_description() + "\nYou already set this item";

//        different pop ups with different confirmation messages and different actions depending on what happens when "yes" is clicked
        buy_popup = new PopUpMessage(storeItem.getName(), buy_confirmation_message, new ButtonAction() {
            @Override
            public void onClick() {
                attemptToBuyItem();
            }
        }, new ButtonAction() {
            @Override
            public void onClick() {

            }
        }, PopUpScreenButtonOptions.yesAndNo);

        set_item_popup = new PopUpMessage(storeItem.getName(), set_confirmation_message, new ButtonAction() {
            @Override
            public void onClick() {
                setItemForUse();
            }
        }, new ButtonAction() {
            @Override
            public void onClick() {

            }
        }, PopUpScreenButtonOptions.yesAndNo);

        already_set_popup = new PopUpMessage(storeItem.getName(), already_set_message, new ButtonAction() {
            @Override
            public void onClick() {
//                already set so nothing needed
            }
        }, new ButtonAction() {
            @Override
            public void onClick() {

            }
        }, PopUpScreenButtonOptions.OK);

        this.addActor(bg_entity);
        this.addActor(store_item_selector);

        this.addActor(sample);
        this.addActor(nameText);

        redefineActualPopUp();

        if (isItemSet()) {
            if (storeItem.getItemType() instanceof PieceShape) {
                this.addActor(set_piece_symbol);
                setSelectedPieceSelector(this);
            }
            if (storeItem.getItemType() instanceof CrownType) {
                this.addActor(set_crown_symbol);
                setSelectedCrownTypeSelector(this);
            }
        }


//        show pop up when item is clicked
        redefineClickListener();

        this.setTouchable(Touchable.enabled);
    }

    private static void setSelectedPieceSelector(StoreItemSelector selectedItem) {
        StoreItemSelector.selectedPieceShape = selectedItem;
    }

    private static void setSelectedCrownTypeSelector(StoreItemSelector selectedItem) {
        StoreItemSelector.selectedCrownType = selectedItem;
    }

    private void redefineClickListener() {
        this.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getStage().addActor(actual_popup);
                super.clicked(event, x, y);
            }
        });
    }

    private void redefineActualPopUp() {
        if (!LocalUserData.isStoreItemUnlocked(storeItem)) {
            this.addActor(coin);
            this.addActor(priceText);
            actual_popup = buy_popup;
        } else {
            if (!isItemSet()) {
                actual_popup = set_item_popup;
            } else {
                actual_popup = already_set_popup;
            }
        }
    }

    private void attemptToBuyItem() {
        if (LocalUserData.number_of_coins >= storeItem.getPrice()) {
            buyItem();

            setItemForUse();

        } else {
            getStage().addActor(new PopUpMessage("Sorry!", "You cannot afford that item yet", new ButtonAction() {
                @Override
                public void onClick() {

                }
            }, new ButtonAction() {
                @Override
                public void onClick() {

                }
            }, PopUpScreenButtonOptions.OK));
        }
    }

    private void buyItem() {
        this.removeActor(priceText);
        this.removeActor(coin);
        LocalUserData.addOrSubtractNumberOfCoins(-storeItem.getPrice());
        LocalUserData.updateUnlockedItems(storeItem);

        clearListeners();
        redefineActualPopUp();
        redefineClickListener();
    }

    private void setItemForUse() {
        if (storeItem.getItemType() instanceof PieceShape) {
            LocalUserData.updatePieceShape((PieceShape) storeItem.getActual_item());
        }

        if (storeItem.getItemType() instanceof CrownType) {
            LocalUserData.updateCrownType((CrownType) storeItem.getActual_item());
        }

        if (storeItem.getItemType() instanceof PieceShape) {
//        remove it from whoever it is currently on
            removeActor(set_piece_symbol);
//        re-add it to the group
            addActor(set_piece_symbol);
        }

        if (storeItem.getItemType() instanceof CrownType) {
//        remove it from whoever it is currently on
            removeActor(set_crown_symbol);
//        re-add it to the group
            addActor(set_crown_symbol);
        }


        clearListeners();
        redefineActualPopUp();
        redefineClickListener();

        if (storeItem.getItemType() instanceof PieceShape) {
            selectedPieceShape.clearListeners();
            selectedPieceShape.redefineActualPopUp();
            selectedPieceShape.redefineClickListener();

            setSelectedPieceSelector(this);
        }
        if (storeItem.getItemType() instanceof CrownType) {
            selectedCrownType.clearListeners();
            selectedCrownType.redefineActualPopUp();
            selectedCrownType.redefineClickListener();

            setSelectedCrownTypeSelector(this);
        }
    }

    public boolean isItemSet() {
        if (storeItem.getItemType() instanceof PieceShape) {
            return LocalUserData.pieceShape.equals(storeItem.getActual_item());
        }

        if (storeItem.getItemType() instanceof CrownType) {
            return LocalUserData.crownType.equals(storeItem.getActual_item());
        }
        return false;
    }
}
