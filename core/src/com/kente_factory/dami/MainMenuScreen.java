package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.kente_factory.dami.enumerations.Player;
import com.kente_factory.dami.logic.GameScreen;
import com.kente_factory.dami.enumerations.MatchType;

/**
 * Created by wahib on 3/26/2018.
 */

public class MainMenuScreen implements Screen {

    private static Stage stage;
    private final Dami game;

    public static GameScreen onlineGameScreen;

    private Button local_one_player_button;
    private Button local_two_player_button;
    private Button local_white_button;
    private Button local_black_button;
    private Button play_sign_in_or_out_button;
    private Button online_two_player_button;
    private Button view_my_games_button;
    private Button store_button;

    public MainMenuScreen(final Dami game) {
        this.game = game;
        stage = new Stage(new StretchViewport(540, 960));

//        define an online game but all we know is that it is online
//        properties like the cell_entries entries and the current turn will be defined via the platform interface and google play games data
        onlineGameScreen = new GameScreen(game, GameScreen.GAME_OPTIONS.two_player, MatchType.online, null, null);

        defineButtons();

        Utils.playGameMusic();
    }

    @Override
    public void show() {
        Dami.stage_type = Dami.STAGE_TYPE.menu;

        defineButtons();

//        check it if it hasn't been done already
        updateButtonsOnSignInStateChanged(game.getPlatformInterface().isGooglePlayGamesSignedIn());
    }

    private void defineButtons() {
        stage.addActor(new Entity(Assets.mainmenu_bgTexture, 0, 0));

        Gdx.input.setInputProcessor(stage);

        SoundButton soundButton = new SoundButton();

        local_one_player_button = new Button(Assets.one_player_buttonTexture, 118, 620, new ButtonAction() {
            @Override
            public void onClick() {

                Gdx.app.log("DAMI", local_one_player_button.getY() + "");

                if (local_one_player_button.getY() <= 621 && local_one_player_button.getY() >= 618) {
                    local_one_player_button.addAction(Actions.moveBy(0, 100, 0.75f, Interpolation.circleOut));


                    local_white_button.addAction(Actions.moveBy(-100, 0, 0.75f, Interpolation.circleOut));
                    local_white_button.addAction(Actions.fadeIn(0.75f, Interpolation.circleOut));
                    local_black_button.addAction(Actions.moveBy(100, 0, 0.75f, Interpolation.circleOut));
                    local_black_button.addAction(Actions.fadeIn(0.75f, Interpolation.circleOut));

                    local_white_button.setTouchable(Touchable.enabled);
                    local_black_button.setTouchable(Touchable.enabled);
                }
                if (local_one_player_button.getY() >= 718 && local_one_player_button.getY() <= 721) {
                    local_one_player_button.addAction(Actions.moveBy(0, -100, 0.75f, Interpolation.circleOut));

                    local_white_button.addAction(Actions.moveBy(100, 0, 0.75f, Interpolation.circleOut));
                    local_white_button.addAction(Actions.fadeOut(0.75f, Interpolation.circleOut));
                    local_black_button.addAction(Actions.moveBy(-100, 0, 0.75f, Interpolation.circleOut));
                    local_black_button.addAction(Actions.fadeOut(0.75f, Interpolation.circleOut));

                    local_white_button.setTouchable(Touchable.disabled);
                    local_black_button.setTouchable(Touchable.disabled);
                }

            }
        });


//        currently not added to the screen
        local_two_player_button = new Button(Assets.two_player_buttonTexture, 118, 470, new ButtonAction() {
            @Override
            public void onClick() {
                game.setScreen(new GameScreen(game, GameScreen.GAME_OPTIONS.two_player, MatchType.local, null, null));
            }
        });


//        underneath the local one player button
        local_white_button = new Button(Assets.white_buttonTexture, 210, 590, new ButtonAction() {
            @Override
            public void onClick() {
                game.setScreen(new GameScreen(game, GameScreen.GAME_OPTIONS.white, MatchType.local, null, Player.white));
            }
        });
        local_white_button.addAction(Actions.alpha(0));
        local_white_button.setTouchable(Touchable.disabled);


        local_black_button = new Button(Assets.black_buttonTexture, 210, 590, new ButtonAction() {
            @Override
            public void onClick() {
                game.setScreen(new GameScreen(game, GameScreen.GAME_OPTIONS.black, MatchType.local, null, Player.white));
            }
        });
        local_black_button.addAction(Actions.alpha(0));
        local_black_button.setTouchable(Touchable.disabled);


        play_sign_in_or_out_button = new Button(Assets.signin_buttonTexture, 118, 500, new ButtonAction() {
            @Override
            public void onClick() {
                if (game.getPlatformInterface().isGooglePlayGamesSignedIn()) {
                    game.getPlatformInterface().googlePlayGamesSignOut();
                } else {
                    game.getPlatformInterface().googlePlayGamesSignIn();
                }
            }
        });


        online_two_player_button = new Button(Assets.two_player_buttonTexture, 118, 500, new ButtonAction() {
            @Override
            public void onClick() {
                game.getPlatformInterface().showSelectOpponent();
            }
        });
        online_two_player_button.addAction(Actions.alpha(0));
        online_two_player_button.setTouchable(Touchable.disabled);


        view_my_games_button = new Button(Assets.games_buttonTexture, 118, 380, new ButtonAction() {
            @Override
            public void onClick() {
//                this opens activity for viewing all online games
                game.getPlatformInterface().checkGames();
//                this restores a local match
//                game.setScreen(new GameScreen(game, Utils.getOptionsFromPlayer(LocalUserData.getOwnedPlayer()), LocalUserData.getLocalStoredBoard(), LocalUserData.getCurrentPlayer()));
            }
        });
        view_my_games_button.addAction(Actions.alpha(0));
        view_my_games_button.setTouchable(Touchable.disabled);

        store_button = new Button(Assets.store_buttonTexture, 118, 265, new ButtonAction() {
            @Override
            public void onClick() {
                game.setScreen(new StoreScreen(game));
            }
        });
        store_button.addAction(Actions.alpha(0));
        store_button.setTouchable(Touchable.disabled);

        stage.addActor(local_white_button);
        stage.addActor(local_black_button);
        stage.addActor(local_one_player_button);
        stage.addActor(online_two_player_button);
//        stage.addActor(local_two_player_button);
        stage.addActor(view_my_games_button);
        stage.addActor(play_sign_in_or_out_button);
        stage.addActor(store_button);

        stage.addActor(soundButton);

    }

    public static void addSpinner(){
        stage.addActor(new LoadingSpinner());
    }

    public void updateButtonsOnSignInStateChanged(boolean signedIn) {
        Gdx.app.log("DamiAndroid", "updated sign in/sign out state animation");
        if (signedIn) {
            play_sign_in_or_out_button.addAction(Actions.moveTo(93f / 2, 344f / 2, 0.5f, Interpolation.circleOut));
            play_sign_in_or_out_button.setTextureRegion(Assets.signout_buttonTexture);
            online_two_player_button.addAction(Actions.alpha(1, 0.5f));
            view_my_games_button.addAction(Actions.alpha(1, 0.5f));
            store_button.addAction(Actions.alpha(1, 0.5f));

            online_two_player_button.setTouchable(Touchable.enabled);
            view_my_games_button.setTouchable(Touchable.enabled);
            store_button.setTouchable(Touchable.enabled);
        } else {
            play_sign_in_or_out_button.addAction(Actions.moveTo(118, 500, 0.5f, Interpolation.circleOut));
            play_sign_in_or_out_button.setTextureRegion(Assets.signin_buttonTexture);
            online_two_player_button.addAction(Actions.alpha(0));
            view_my_games_button.addAction(Actions.alpha(0));
            store_button.addAction(Actions.alpha(0));

            online_two_player_button.setTouchable(Touchable.disabled);
            view_my_games_button.setTouchable(Touchable.disabled);
            store_button.setTouchable(Touchable.disabled);
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255f, 255f, 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        stage.act();
        stage.draw();
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {
        Assets.reset();
    }

    @Override
    public void hide() {
        for (Actor actor : stage.getActors()) {
            actor.remove();
        }
    }

    @Override
    public void dispose() {

    }
}
