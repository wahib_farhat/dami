package com.kente_factory.dami;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.kente_factory.dami.logic.GameScreen;
import com.kente_factory.dami.logic.Piece;

/**
 * Created by wahib on 12/27/2017.
 */

public class BlackBox extends Entity {

    public int col, row;

    private GameScreen gameScreen;

    public BlackBox(final GameScreen gameScreen, int col, int row) {
        super(Assets.black_boxTexture, col * 50 + 21, row * 50 + 22 + 205);

        this.row = row;
        this.col = col;

        this.setTouchable(Touchable.enabled);

        this.gameScreen = gameScreen;

        addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                SelectIcon icon = new SelectIcon(BlackBox.this, BlackBox.this.getX(), BlackBox.this.getY());
                BlackBox.this.getStage().addActor(icon);
                if(GameScreen.getSelectedPiece() != null) {
                    icon.setZIndex(GameScreen.getSelectedPiece().getZIndex() - 1);
                }
                super.touchUp(event, x, y, pointer, button);
                gameScreen.setUser_destination(new int[]{BlackBox.this.row, BlackBox.this.col});
//                clear the selection so it is set as null before next turn starts
                Piece.clearSelect();
            }
        });
    }
}
