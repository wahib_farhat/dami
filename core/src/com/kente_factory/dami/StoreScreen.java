package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class StoreScreen implements Screen {

    private final Stage stage;
    private final Dami game;

    public StoreScreen(Dami game) {
        this.game = game;
        stage = new Stage(new StretchViewport(540, 960));

        defineButtons();

    }


    private void defineButtons() {
        stage.addActor(new Entity(Assets.store_bgTexture, 0, 0));

        stage.addActor(new BackButton(new ButtonAction() {
            @Override
            public void onClick() {
                game.setScreen(Dami.menuScreen);
            }
        }));

        Entity coins_bg = new Entity(Assets.coins_bgTexture, 347f / 2, (1920f - 152.2f) / 2);
        stage.addActor(coins_bg);

        final Text scoreText = new Text(LocalUserData.number_of_coins + "", 0, 934, 0.5f, Color.WHITE, 540 + 35, true){
            @Override
            public void draw(Batch batch, float parentAlpha) {
                if(getLetters().length() == 6) {
                    setScale(0.4f);
                }
                if(getLetters().length() >= 7) {
                    setScale(0.35f);
                }

                setLetters(LocalUserData.number_of_coins + "");

                super.draw(batch, parentAlpha);
            }
        };


        stage.addActor(scoreText);

//        piece scroller



        Table piece_scrollTable = new Table();
        piece_scrollTable.add(Dami.circlePieceSelector).padLeft(20);
        piece_scrollTable.add(Dami.diamondPieceSelector).padLeft(20);
        piece_scrollTable.add(Dami.squarePieceSelector).padLeft(20);
        piece_scrollTable.add(Dami.trianglePieceSelector).padLeft(20);

        final ScrollPane piece_scroller = new ScrollPane(piece_scrollTable);
        final Table piece_table = new Table();
        piece_table.setPosition(0, (1920f - 776f) / 2);
        piece_table.setSize(Dami.diamondPieceSelector.getWidth() * 4, Dami.diamondPieceSelector.getHeight());
        piece_table.add(piece_scroller);

        this.stage.addActor(piece_table);

//        crown scroller

        Table crown_scrollTable = new Table();
        crown_scrollTable.add(Dami.normalCrownSelector).padLeft(20);
        crown_scrollTable.add(Dami.queenCrownSelector).padLeft(20);
        crown_scrollTable.add(Dami.kingCrownSelector).padLeft(20);
        crown_scrollTable.add(Dami.bowlingHatCrownSelector).padLeft(20);

        final ScrollPane crown_scroller = new ScrollPane(crown_scrollTable);
        final Table crown_table = new Table();
        crown_table.setPosition(0, (1920f - 1450f) / 2);
        crown_table.setSize(Dami.diamondPieceSelector.getWidth() * 4, Dami.diamondPieceSelector.getHeight());
        crown_table.add(crown_scroller);

        this.stage.addActor(crown_table);

        Gdx.input.setInputProcessor(stage);
    }


    @Override
    public void show() {
        Dami.stage_type = Dami.STAGE_TYPE.store;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(255f, 255f, 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        stage.act();
        stage.draw();
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        for (Actor actor : stage.getActors()) {
            actor.remove();
        }
    }

    @Override
    public void dispose() {

    }
}
