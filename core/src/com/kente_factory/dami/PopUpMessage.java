package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.kente_factory.dami.enumerations.PopUpScreenButtonOptions;

/**
 * a pop up message that asks you if you are sure of an action
 *
 * options - if you choose OK option, only one button will be shown, YES
 */
public class PopUpMessage extends Group {

    public PopUpMessage(String message_name, String message_body, final ButtonAction yesButtonAction, final ButtonAction noButtonAction, PopUpScreenButtonOptions options) {

        float x = 57f / 2;
        float y = 478f / 2;


        Pixmap background = new Pixmap(965, 965, Pixmap.Format.RGBA8888);
        background.setColor(new Color(196f / 225, 154f / 225, 108f / 225, 1));
        background.fill();

        Entity bg = new Entity(new Texture(background), x, y);
        Button yes_button = new Button(Assets.yes_buttonTexture, x + 120, y + 100, new ButtonAction() {
            @Override
            public void onClick() {
                PopUpMessage.this.remove();
                yesButtonAction.onClick();
            }
        });
        final Button no_button = new Button(Assets.no_buttonTexture, x + 270, y + 100, new ButtonAction() {
            @Override
            public void onClick() {
                PopUpMessage.this.remove();
                noButtonAction.onClick();
            }
        });

        this.addActor(bg);
        this.addActor(yes_button);

        if(!options.equals(PopUpScreenButtonOptions.OK)) {
            this.addActor(no_button);
        } else {
            yes_button.setPosition(x + 200, yes_button.getY());
        }

        Text nameText = new Text(message_name, 0, 700, 0.5f, Color.BROWN, 540, true);
        Text messageText = new Text(message_body, 0, 600, 0.3f, Color.BROWN, 540, true);

        this.addActor(nameText);
        this.addActor(messageText);
    }
}
