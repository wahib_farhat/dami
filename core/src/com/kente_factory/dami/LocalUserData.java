package com.kente_factory.dami;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import com.kente_factory.dami.enumerations.store_item_types.CrownType;
import com.kente_factory.dami.enumerations.store_item_types.PieceShape;
import com.kente_factory.dami.enumerations.CellEntry;
import com.kente_factory.dami.enumerations.Player;


/**
 * Wahib
 *
 * Local persistent data that will be used in case the person reopens the app and wants to continue a stored game on local storage
 */
public class LocalUserData {

    private static Preferences prefs = Gdx.app.getPreferences("Dami");

    public static int number_of_coins;

    public static PieceShape pieceShape;

    public static CrownType crownType;

    private static Array<Integer> default_unlocked_items;

    private static String unlocked_items_json_string;

    public static boolean sound_enabled;

    //    the local json strings are not used. just converted and used in the GameScreen
    private static String local_board_data_json;

    private static String local_owned_player_string;

    private static String local_current_turn_string;


    public static void initialize() {

//        separating the local json states from what will be received from online
        // put in the stored board or give him the default board
        local_board_data_json = prefs.getString("local_board",
                Utils.json_instance.toJson(Utils.getDefaultBoard()));

        local_owned_player_string = prefs.getString("local_owned_player_string", "null");
        local_current_turn_string = prefs.getString("local_current_turn_string", "null");


        String pieceShapeString = prefs.getString("piece_shape", "circle");
        pieceShape = PieceShape.valueOf(pieceShapeString);

        String crownTypeString = prefs.getString("crown_type", "normal");
        crownType = CrownType.valueOf(crownTypeString);

        sound_enabled = prefs.getBoolean("sound_enabled", true);

        number_of_coins = prefs.getInteger("number_of_coins", 5000);

        default_unlocked_items = new Array<Integer>(new Integer[]{Dami.circlePiece.getId(), Dami.normalCrown.getId()});

        unlocked_items_json_string = prefs.getString("unlocked_items", Utils.json_instance.toJson(default_unlocked_items));
        Gdx.app.log("items_string", unlocked_items_json_string);

//        testing
//        updatePieceShape(PieceShape.circle);
//        updateCrownType(CrownType.king);
//        addOrSubtractNumberOfCoins(5000);
//        updateUnlockedItems(Dami.diamondPiece);
    }


    public static boolean isStoreItemUnlocked(StoreItem storeItem) {
        Array<Integer> unlocked_items = Utils.json_instance.fromJson(null, unlocked_items_json_string);

        return unlocked_items.contains(storeItem.getId(), true);
    }

    public static boolean updateUnlockedItems(StoreItem storeItem) {
        if(!isStoreItemUnlocked(storeItem)) {
            Array<Integer> unlocked_items = Utils.json_instance.fromJson(null, unlocked_items_json_string);
            unlocked_items.add(storeItem.getId());
            unlocked_items_json_string = Utils.json_instance.toJson(unlocked_items);

            prefs.putString("unlocked_items", unlocked_items_json_string);
            prefs.flush();

            return true;
        }
        return false;
    }

    public static void updateSoundEnabled(boolean sound_enabled) {
        prefs.putBoolean("sound_enabled", sound_enabled);
        prefs.flush();

        LocalUserData.sound_enabled = prefs.getBoolean("sound_enabled", true);
    }

    public static void updatePieceShape(PieceShape pieceShape) {
        String pieceShapeString = pieceShape.toString();
        prefs.putString("piece_shape", pieceShapeString);
        prefs.flush();

        pieceShapeString = prefs.getString("piece_shape", "circle");
        LocalUserData.pieceShape = PieceShape.valueOf(pieceShapeString);
    }

    public static void updateCrownType(CrownType crownType) {
        String crownTypeString = crownType.toString();
        prefs.putString("crown_type", crownTypeString);
        prefs.flush();

        crownTypeString = prefs.getString("crown_type", "normal");
        LocalUserData.crownType = CrownType.valueOf(crownTypeString);
    }


    public static void addOrSubtractNumberOfCoins(int number_of_coins) {
        LocalUserData.number_of_coins += number_of_coins;
        prefs.putInteger("number_of_coins", LocalUserData.number_of_coins);
        prefs.flush();

        LocalUserData.number_of_coins = prefs.getInteger("number_of_coins", 0);
    }

    public static Player getLocalOwnedPlayer() {
        return Utils.getPlayerFromString(local_owned_player_string);
    }

    public static Player getLocalCurrentPlayer() {
        return Utils.getPlayerFromString(local_current_turn_string);
    }

    public static void updateLocalOwnedPlayer(Player player) {
        switch (player) {
            case black:
                prefs.putString("local_owned_player_string", "black");
                prefs.flush();
                break;
            case white:
                prefs.putString("local_owned_player_string", "white");
                prefs.flush();
                break;
            default:
//        something's wrong
                prefs.putString("local_owned_player_string", "null");
                prefs.flush();
        }

        local_owned_player_string = prefs.getString("local_owned_player_string", "null");
    }

    public static void updateLocalCurrentPlayer(Player player) {
        switch (player) {
            case black:
                prefs.putString("local_current_turn_string", "black");
                prefs.flush();
                break;
            case white:
                prefs.putString("local_current_turn_string", "white");
                prefs.flush();
                break;
            default:
//        something's wrong
                prefs.putString("local_current_turn_string", "null");
                prefs.flush();
        }
    }


    public static void updateLocalBoard(CellEntry[][] new_board) {

        local_board_data_json = Utils.json_instance.toJson(new_board);

        prefs.putString("local_board", local_board_data_json);
        prefs.flush();
    }

}
