package com.kente_factory.dami;

public interface PlatformInterface {

    void googlePlayGamesSignIn();

    void googlePlayGamesSignOut();

    void showSelectOpponent();

    void checkGames();

    void updateOnlineMatch(boolean finish);

    void initiateRematch();

    void endOnlineMatch();

    boolean isGooglePlayGamesSignedIn();

    boolean isWhiteOwner();
}
