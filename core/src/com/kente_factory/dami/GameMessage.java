package com.kente_factory.dami;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.kente_factory.dami.enumerations.MatchType;
import com.kente_factory.dami.enumerations.Player;
import com.kente_factory.dami.logic.GameScreen;

/**
 * Created by wahib on 5/19/2018.
 */

public class GameMessage extends Group {

    /**
     * @param game_options what the player chose
     * @param winner       who won?
     */
    public GameMessage(final com.kente_factory.dami.enumerations.MatchType matchType, final GameScreen.GAME_OPTIONS game_options, com.kente_factory.dami.enumerations.Player winner, final Dami game) {

        float x = 57f / 2;
        float y = 478f / 2;


        Entity bg = new Entity(Utils.game_message_backgroundTexture, x, y);
        Entity black_bg = new Entity(Utils.dark_bgTexture, 0, 0);

        Entity yes_button = new Entity(Assets.yes_buttonTexture, x + 120, y + 100);
        Entity no_button = new Entity(Assets.no_buttonTexture, x + 270, y + 100);

        yes_button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                if(matchType.equals(com.kente_factory.dami.enumerations.MatchType.local)) {
                    game.setScreen(new GameScreen(game, game_options, com.kente_factory.dami.enumerations.MatchType.local, null, Player.white));
                }
                if(matchType.equals(MatchType.online)) {
//                  the current match will be ended if not already
                    game.getPlatformInterface().endOnlineMatch();
                    game.getPlatformInterface().showSelectOpponent();
                }

                GameMessage.this.remove();
            }
        });

        no_button.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
//              the current match will be ended if not already
                game.getPlatformInterface().endOnlineMatch();
                game.setScreen(new MainMenuScreen(game));
                GameMessage.this.remove();
            }
        });

        this.addActor(black_bg);
        this.addActor(bg);
        this.addActor(yes_button);
        this.addActor(no_button);

        Texture messageTexture = Assets.player_lost_messageTexture;

//        use "black won" or "white won" messages only
        if (winner == Player.white) {
            // white won
            messageTexture = Assets.white_won_messageTexture;
        } else {
            // black won
            messageTexture = Assets.black_won_messageTexture;
        }


//        draw condition
        if (winner == null) {
            messageTexture = Assets.draw_messageTexture;
        }

        Entity messageEntity = new Entity(messageTexture, x + 50f, y + 270f);
        this.addActor(messageEntity);
    }
}
