package com.kente_factory.dami;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class SoundButton extends Button {

    public SoundButton() {
        super(Assets.sound_buttonTexture, 2, 1, 826.6f / 2, 344f / 2);

        if(!LocalUserData.sound_enabled) {
            this.setTextureRegion(1);
        }


        final ButtonAction buttonAction = new ButtonAction() {
            @Override
            public void onClick() {
                if(LocalUserData.sound_enabled) {

                    LocalUserData.updateSoundEnabled(false);
                    SoundButton.this.setTextureRegion(1);

                    Utils.pauseGameMusic();

                } else {

                    LocalUserData.updateSoundEnabled(true);
                    SoundButton.this.setTextureRegion(0);

                    Utils.playGameMusic();

                }
            }
        };

        this.setButtonAction(buttonAction);

    }

    private void toggleSound() {
        if(LocalUserData.sound_enabled) {

            LocalUserData.updateSoundEnabled(false);
            this.setTextureRegion(1);

            Utils.pauseGameMusic();

        } else {

            LocalUserData.updateSoundEnabled(true);
            this.setTextureRegion(0);

            Utils.playGameMusic();

        }
    }
}
