package com.kente_factory.dami;

import com.badlogic.gdx.graphics.Texture;

public class StoreItem {

    private String name;
    private String short_description;
    private String long_description;

    private int price;
    private int id;
    private Texture sampleTexture;

    private Object actual_item;

    /**
     *
     * @param id unique id of the item
     * @param actual_item currently only enums, but Object type allows us to expand this if need be
     * @param name string with short name of the item. recommended two words
     * @param short_description short string describing item
     * @param long_description log string describing item
     * @param price amount to be deducted from user's coins when buying item
     * @param sampleTexture texture to be used to represent it in the store
     */
    public StoreItem(int id, Object actual_item, String name, String short_description, String long_description, int price, Texture sampleTexture) {
        this.id = id;
        this.name = name;
        this.short_description = short_description;
        this.long_description = long_description;
        this.price = price;
        this.sampleTexture = sampleTexture;
        this.actual_item = actual_item;

    }

    public Object getActual_item() {
        return actual_item;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getShort_description() {
        return short_description;
    }

    public String getLong_description() {
        return long_description;
    }

    public int getPrice() {
        return price;
    }

    public Object getItemType() {
        return actual_item;
    }

    public Texture getSampleTexture() {
        return sampleTexture;
    }

}
