package com.kente_factory.dami;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by wahib on 12/27/2017.
 */

public class Entity extends Actor {

    private TextureRegion region;
    private TextureRegion[] regions;

    public Entity(Texture texture, float x, float y) {

        this.setPosition(x, y);

        if (texture != null) {
            this.region = new TextureRegion(texture);

            this.setSize(this.region.getRegionWidth() / 2, this.region.getRegionHeight() / 2);
            this.setOrigin(this.getWidth() / 2, this.getHeight() / 2);
        }
    }

    public Entity(Texture texture, int FRAME_COLS, int FRAME_ROWS, float x, float y) {

        // two dimensional array of regions of the Entity
        TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()
                / FRAME_COLS, texture.getHeight() / FRAME_ROWS);
        // define and fill in the regions array with the necessary texture
        // regions. note it is one-dimensional
        regions = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                regions[index++] = tmp[i][j];
            }
        }

        this.setPosition(x, y);

        this.region = regions[0];
        this.setSize(this.region.getRegionWidth() / 2, this.region.getRegionHeight() / 2);
        this.setOrigin(this.getWidth() / 2, this.getHeight() / 2);
    }

    public void setTextureRegion(Texture texture){
        this.region = new TextureRegion(texture);
        this.setSize(this.region.getRegionWidth() / 2, this.region.getRegionHeight() / 2);
    }

    public void setTextureRegion(int frame){
        this.region = regions[frame];
    }

    public TextureRegion getRegion() {
        return region;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        if (region != null) {
            batch.draw(region, this.getX(), this.getY(), this.getOriginX(),
                    this.getOriginY(), this.getWidth(), this.getHeight(),
                    this.getScaleX(), this.getScaleY(), this.getRotation());
        }
        this.setBounds(this.getX(), this.getY(), this.getWidth(),
                this.getHeight());
        super.draw(batch, parentAlpha);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }
}
