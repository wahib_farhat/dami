package com.kente_factory.dami;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;

/**
 * Created by wahib on 12/28/2017.
 */

public class SelectIcon extends Entity {

    public SelectIcon(Entity selectedEntity, float x, float y) {
        super(Assets.icon_selectTexture, x, y);

        if(selectedEntity instanceof BlackBox){
            this.addAction(Actions.sequence(Actions.fadeOut(0.4f), Actions.run(new Runnable() {
                @Override
                public void run() {
                    SelectIcon.this.remove();
                }
            })));
        }
    }
}
