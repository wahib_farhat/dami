package com.kente_factory.dami.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.kente_factory.dami.Dami;
import com.kente_factory.dami.PlatformInterface;

public class DesktopLauncher implements PlatformInterface {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 270;
		config.height = 480;
		new LwjglApplication(new Dami(new DesktopLauncher()), config);
	}

	@Override
	public void googlePlayGamesSignIn() {
        Dami.menuScreen.updateButtonsOnSignInStateChanged(true);
	}

	@Override
	public void googlePlayGamesSignOut() {

	}

	@Override
	public void showSelectOpponent() {

	}

	@Override
	public void checkGames() {

	}

	@Override
	public void updateOnlineMatch(boolean finish) {

	}

//	@Override
//	public void finishOnlineMatch() {
//
//	}

	@Override
	public void initiateRematch() {

	}

	@Override
	public boolean isGooglePlayGamesSignedIn() {
		return false;
	}

	@Override
	public boolean isWhiteOwner() {
		return false;
	}
}
